package com.atlassian.graphql.json.jersey;

import com.atlassian.graphql.GraphQLFetcherException;
import com.atlassian.graphql.datafetcher.MethodDataFetcher;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.google.common.base.Defaults;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Primitives;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;

/**
 * A data fetcher that invokes a method using Jersey parameter semantics:
 * - List, Set and SortedSet collection parameter types are supported (default is an empty collection)
 * - A static method named 'valueOf' is used to convert parameter values, if present
 * - Numeric values are converted to each other appropriately
 * Jersey Response objects are unwrapped before being returned.
 */
public class JerseyResourceMethodDataFetcher extends MethodDataFetcher {
    private final List<ArgumentConverter> argumentConverters;
    private Method responseGetEntityMethod;
    private Method responseGetStatusMethod;

    static final List<ArgumentConverter> DEFAULT_ARGUMENT_CONVERTERS = ImmutableList.<ArgumentConverter>builder()
            .add(new SameTypeArgumentConverter())
            .add(new EnumArgumentConverter())
            .add(new NumberArgumentConverter())
            .add(new ValueOfMethodArgumentConverter())
            .build();

    public JerseyResourceMethodDataFetcher(
            final Method method,
            final String fieldName,
            final Object source,
            final GraphQLOutputType responseType,
            final List<String> currentFieldPath,
            final Map<String, GraphQLType> allTypes,
            final GraphQLExtensions extensions,
            final List<ArgumentConverter> argumentConverters) {

        super(method, fieldName, source, responseType, currentFieldPath, allTypes, extensions);
        this.argumentConverters = ImmutableList.copyOf(argumentConverters);


        if (isJerseyResponseType(method.getReturnType())) {
            try {
                responseGetEntityMethod = method.getReturnType().getMethod("getEntity");
                responseGetStatusMethod = method.getReturnType().getMethod("getStatus");
            } catch (final NoSuchMethodException ex) {
            }
        }
    }

    @Override
    public Object get(final DataFetchingEnvironment env) {
        return unwrapJerseyResponse(env, super.get(env));
    }

    /**
     * If the response is a javax.ws.rs.core.Response, unwrap the response by calling Response.getEntity().
     */
    private Object unwrapJerseyResponse(final DataFetchingEnvironment env, final Object response) {
        if (responseGetEntityMethod != null && response != null) {
            try {
                Object result = responseGetEntityMethod.invoke(response);
                if (responseGetStatusMethod != null) {
                    int status = (int) responseGetStatusMethod.invoke(response);
                    result = transformResponseStatus(env, result, status);
                }
                return result;
            } catch (ReflectiveOperationException ex) {
                throw Throwables.propagate(ex);
            }
        }
        return response;
    }

    /**
     * Override to return a different response or throw an exception based on the value of Response.getStatus().
     */
    protected Object transformResponseStatus(final DataFetchingEnvironment env, final Object response, final int status) {
        return response;
    }

    private static boolean isJerseyResponseType(final Class responseType) {
        final String name = responseType.getName();
        return name.endsWith(".Response")
                || (name.startsWith("javax.ws.") && name.startsWith("com.atlassian.graphql.mock.jersey."));
    }

    @Override
    protected Object getDefaultValue(final Type type) {
        return getDefault(getClazz(type));
    }

    // this is used in Confluence
    public static Object getDefault(final Type type) {
        return getDefault(getClazz(type));
    }

    private static Object getDefault(Class<?> clazz) {
        return Collection.class.isAssignableFrom(clazz)
                ? constructCollection(clazz)
                : Defaults.defaultValue(clazz);
    }

    @Override
    protected Object convertFieldArgumentValue(final DataFetchingEnvironment env, final Object value, final Type type) {
        return convertArgumentValue(env, super.convertFieldArgumentValue(env, value, type), type);
    }

    Object convertArgumentValue(final DataFetchingEnvironment env, final Object value, final Type type) {
        final Class<?> clazz = Primitives.wrap(getClazz(type));
        if (value == null) {
            return getDefault(clazz);
        }

        // convert a Collection value
        if (Collection.class.isAssignableFrom(clazz)) {
            return convertCollectionFieldArgumentValue(env, (Collection) value, type);
        }

        try {
            return argumentConverters.stream()
                    .filter(ac -> ac.test(clazz, value))
                    .findFirst()
                    .map(ac -> ac.apply(clazz, value))
                    .orElseThrow(() -> new GraphQLFetcherException(env, "Unsupported parameter conversion for type '" + type.getTypeName() + "'"));
        } catch (ArgumentConversionException e) {
            throw new GraphQLFetcherException(env, e);
        }
    }

    private Object convertCollectionFieldArgumentValue(final DataFetchingEnvironment env, final Collection<?> value, final Type type) {
        final Collection<Object> result = constructCollection(getClazz(type));
        for (Object item : value) {
            result.add(convertArgumentValue(env, item, ((ParameterizedType) type).getActualTypeArguments()[0]));
        }
        return result;
    }

    private static Collection<Object> constructCollection(final Class clazz) {
        if (List.class.isAssignableFrom(clazz)) {
            return new ArrayList<>();
        } else if (Set.class.isAssignableFrom(clazz)) {
            return new HashSet<>();
        } else if (SortedSet.class.isAssignableFrom(clazz)) {
            return new TreeSet<>();
        }
        throw new RuntimeException("Unsupported collection type '" + clazz.getName() + "'");
    }
}
