package com.atlassian.graphql.router;

import com.atlassian.graphql.rest.GraphQLRestRequest;
import com.atlassian.graphql.utils.GraphQLUtils;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;

import java.util.concurrent.CompletableFuture;

import static graphql.ExecutionInput.newExecutionInput;

/**
 * A GraphQL route that runs the query over a local {@link GraphQLSchema}.
 */
public class LocalGraphQLRoute implements GraphQLRoute {
    private final GraphQLRouteRules rules;
    private GraphQLSchema schema;
    private GraphQL graphql;

    public LocalGraphQLRoute(final GraphQLRouteRules rules, final GraphQLSchema schema) {
        this.rules = rules;
        this.schema = schema;
    }

    public LocalGraphQLRoute(final GraphQLRouteRules rules, final GraphQL graphql) {
        this.rules = rules;
        this.graphql = graphql;
    }

    @Override
    public GraphQLRouteRules getRouteRules() {
        return rules;
    }

    @Override
    public CompletableFuture<ExecutionResult> route(final GraphQLRestRequest request, Object context) {
        ExecutionInput executionInput = newExecutionInput()
                .query(request.getQuery())
                .operationName(request.getOperationName())
                .context(context)
                .variables(request.getVariables())
                .build();
        return graphql != null
                ? graphql.executeAsync(executionInput)
                : GraphQLUtils.executeDocument(schema, request.getQueryDocument(), executionInput);
    }
}
