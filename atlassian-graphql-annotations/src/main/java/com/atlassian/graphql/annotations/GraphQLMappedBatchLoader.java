package com.atlassian.graphql.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks methods that provide MappedBatchLoader instances
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphQLMappedBatchLoader {

    /**
     * @return The key of the DataLoader registration.  If not specified, the method name is used.
     */
    String value() default "";
}
