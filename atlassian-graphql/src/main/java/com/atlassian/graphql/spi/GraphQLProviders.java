package com.atlassian.graphql.spi;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Provides a set of graphql providers for a plugin at a specific path.
 */
public class GraphQLProviders {
    private final String path;
    private final List<Object> providers;
    private final GraphQLExtensions extensions;

    public GraphQLProviders(final String path, final Object provider) {
        this(path, Collections.singletonList(provider));
    }

    public GraphQLProviders(final String path, final List<Object> providers) {
        this(path, providers, null);
    }

    public GraphQLProviders(final String path, final List<Object> providers, final GraphQLExtensions extensions) {
        this.path = path;
        this.providers = requireNonNull(providers);
        this.extensions = extensions;
    }

    public String getPath() {
        return path;
    }

    public List<Object> getProviders() {
        return providers;
    }

    public GraphQLExtensions getExtensions() {
        return extensions;
    }
}
