package com.atlassian.graphql.provider;

import com.atlassian.graphql.annotations.GraphQLIDType;
import com.atlassian.graphql.annotations.GraphQLMutation;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLEnumType;
import graphql.schema.GraphQLInputType;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLNamedInputType;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLScalarType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RootProviderGraphQLTypeBuilderTest {

    @Test
    public void testMutationInputTypes() {
        GraphQLSchema schema = testBuildSchema(new TestProviderMutationInputTypeUsedMultipleTimes(), true, true);

        GraphQLObjectType providerType = (GraphQLObjectType) schema.getMutationType().getFieldDefinitions().get(0).getType();

        GraphQLNamedInputType simpleArgumentType = (GraphQLNamedInputType) getArgumentType(providerType, "simple");
        assertEquals("String", simpleArgumentType.getName());

        GraphQLNamedInputType simple2ArgumentType = (GraphQLNamedInputType) getArgumentType(providerType, "simple2");
        assertEquals("ID", simple2ArgumentType.getName());

        GraphQLNamedInputType enumArgumentType = (GraphQLNamedInputType) getArgumentType(providerType, "enumType");
        assertEquals("EnumType", enumArgumentType.getName());

        GraphQLNamedInputType enum2ArgumentType = (GraphQLNamedInputType) getArgumentType(providerType, "enumType2");
        assertEquals("EnumType", enum2ArgumentType.getName());

        GraphQLNamedInputType objectArgumentType = (GraphQLNamedInputType) getArgumentType(providerType, "object");
        assertEquals("InputMessage", objectArgumentType.getName());

        graphql.schema.GraphQLNonNull object2ArgumentType = (graphql.schema.GraphQLNonNull) getArgumentType(providerType, "object2");
        assertEquals("InputMessage", ((GraphQLNamedInputType) object2ArgumentType.getWrappedType()).getName());
    }

    @Test
    public void testMutationListInputTypes() {
        GraphQLSchema schema = testBuildSchema(new TestProviderListArgumentMutation(), true, true);

        GraphQLObjectType providerType = (GraphQLObjectType) schema.getMutationType().getFieldDefinitions().get(0).getType();

        GraphQLInputType simpleListArgumentType = getArgumentType(providerType, "simple");
        assertTrue(simpleListArgumentType instanceof GraphQLList);
        GraphQLType simpleArgumentType = ((GraphQLList) simpleListArgumentType).getWrappedType();
        assertTrue(simpleArgumentType instanceof GraphQLScalarType);
        assertEquals("String", ((GraphQLScalarType) simpleArgumentType).getName());

        GraphQLInputType enumListArgumentType = getArgumentType(providerType, "enumType");
        assertTrue(enumListArgumentType instanceof GraphQLList);
        GraphQLType enumArgumentType = ((GraphQLList) enumListArgumentType).getWrappedType();
        assertTrue(enumArgumentType instanceof GraphQLEnumType);
        assertEquals("EnumType", ((GraphQLEnumType) enumArgumentType).getName());

        GraphQLInputType objectListArgumentType = getArgumentType(providerType, "object");
        assertTrue(objectListArgumentType instanceof GraphQLList);
        GraphQLType objectArgumentType = ((GraphQLList) objectListArgumentType).getWrappedType();
        assertTrue(objectArgumentType instanceof GraphQLNamedInputType);
        assertEquals("InputMessage", ((GraphQLNamedInputType) objectArgumentType).getName());
    }

    private GraphQLInputType getArgumentType(GraphQLObjectType providerType, String name) {
        return providerType.getFieldDefinition(name).getArgument(name).getType();
    }

    @Test
    public void testBuildSchema() {
        testBuildSchema(new TestProviderQuery(), true, false);
        testBuildSchema(new TestProviderQueryAndMutation(), true, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProviderWithNoFields() {
        testBuildSchema(new TestProviderNoFields(), false, false);
    }

    private GraphQLSchema testBuildSchema(final Object provider, final boolean hasQueryType, final boolean hasMutationType) {
        final RootProviderGraphQLTypeBuilder providerTypeBuilder = new RootProviderGraphQLTypeBuilder();
        final List<GraphQLProviders> providers = Lists.newArrayList(new GraphQLProviders("field", provider));

        final GraphQLSchema schema = providerTypeBuilder.buildSchema("rootQuery", "rootMutation", providers, null);
        assertEquals("Has query type", hasQueryType, schema.getQueryType() != null);
        assertEquals("Has mutation type", hasMutationType, schema.getMutationType() != null);

        return schema;
    }

    public enum EnumType {
        VALUE1, VALUE2
    }

    @GraphQLProvider
    public static class TestProviderQuery {
        @GraphQLName
        public String get() {
            return "value";
        }
    }

    @GraphQLProvider
    public static class TestProviderQueryAndMutation {
        @GraphQLName
        public String get() {
            return "value";
        }

        @GraphQLName("simple")
        @GraphQLMutation
        public String simple(@GraphQLName("simple") String id) {
            return "value";
        }

        @GraphQLName("enumType")
        @GraphQLMutation
        public EnumType enumType(@GraphQLName("enumType") EnumType enumType) {
            return enumType;
        }

        @GraphQLName("object")
        @GraphQLMutation
        public String object(@GraphQLName("object") InputMessage input) {
            return "value";
        }
    }

    @GraphQLProvider
    public static class TestProviderListArgumentMutation {
        @GraphQLName
        public String get() {
            return "value";
        }

        @GraphQLName("simple")
        @GraphQLMutation
        public String simple(@GraphQLName("simple") List<String> id) {
            return "value";
        }

        @GraphQLName("enumType")
        @GraphQLMutation
        public String enumType(@GraphQLName("enumType") List<EnumType> enumType) {
            return "value";
        }

        @GraphQLName("object")
        @GraphQLMutation
        public String object(@GraphQLName("object") List<InputMessage> input) {
            return "value";
        }
    }

    @GraphQLProvider
    public static class TestProviderMutationInputTypeUsedMultipleTimes extends TestProviderQueryAndMutation {

        @GraphQLName("simple2")
        @GraphQLMutation
        public String simple2(@GraphQLName("simple2") @GraphQLIDType String id) {
            return "value";
        }

        @GraphQLName("enumType2")
        @GraphQLMutation
        public EnumType enumType2(@GraphQLName("enumType2") EnumType enumType) {
            return enumType;
        }

        @GraphQLName("object2")
        @GraphQLMutation
        public String object2(@GraphQLName("object2") @GraphQLNonNull InputMessage input) {
            return "value";
        }
    }

    public static class BaseInputMessage {
        @GraphQLName
        private String inputField;

        protected BaseInputMessage() {
        }

        public BaseInputMessage(String inputField) {
            this.inputField = inputField;
        }
    }

    public static class InputMessage extends BaseInputMessage {
        public InputMessage() {
        }

        public InputMessage(String inputField) {
            super(inputField);
        }
    }

    @GraphQLProvider
    public static class TestProviderNoFields { }
}
