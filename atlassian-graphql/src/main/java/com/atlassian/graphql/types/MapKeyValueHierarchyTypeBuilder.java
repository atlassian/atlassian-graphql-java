package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.ReflectionUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A GraphQLList builder for a hierarchy of key/value pair Map objects.
 */
public class MapKeyValueHierarchyTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;

    public MapKeyValueHierarchyTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = requireNonNull(typeBuilder);
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        final String typeName = ReflectionUtils.getAnnotatedGraphQLTypeName(type, element);
        return typeName != null ? typeName : "KeyValueHierarchyMap";
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        if (!MapTypeBuilder.isMapType(type)) {
            return false;
        }
        final ParameterizedType parameterizedType = (ParameterizedType) type;
        return parameterizedType.getActualTypeArguments()[0] == String.class
               && parameterizedType.getActualTypeArguments()[1] == Object.class;
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);

        final Type listTypeParameter = new DynamicType(
                typeName,
                ImmutableMap.of("key", String.class,
                                "value", String.class,
                                "fields", type));

        final GraphQLType listElementType = typeBuilder.buildType(typeName, listTypeParameter, element, context);
        return new GraphQLList(listElementType);
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return this::mapToKeyValueList;
    }

    private Object mapToKeyValueList(final Object obj) {
        if (!(obj instanceof Map)) {
            return null;
        }
        final Map<String, Object> map = (Map<String, Object>) obj;

        final List<Map<String, Object>> list = new ArrayList<>();
        for (final Map.Entry<String, Object> entry : map.entrySet()) {
            list.add(makeKeyValue(entry.getKey(), entry.getValue()));
        }
        return list;
    }

    private Map<String, Object> makeKeyValue(final String key, final Object value) {
        final Map<String, Object> map = Maps.newHashMap();
        map.put("key", key);
        map.put(value instanceof Map ? "fields" : "value",
                value instanceof Map ? value : toString(value));
        return map;
    }

    private String toString(final Object value) {
        final Function<Object, Object> transformer =
                typeBuilder.getValueTransformer(value.getClass(), null);
        final Object obj = transformer != null ? transformer.apply(value) : value;
        return obj != null ? obj.toString() : null;
    }
}
