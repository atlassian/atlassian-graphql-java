package com.atlassian.graphql.rest;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.annotations.GraphQLMutation;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GraphQLRestServerTest {
    @SuppressWarnings("unchecked")
    @Test
    public void testExecute() throws Exception {
        final Provider provider = new Provider();
        final GraphQLRestServer endpoint =
                GraphQLRestServer.builder()
                .providers(Lists.newArrayList(new GraphQLProviders("root", provider)))
                .build();

        final String request = "{'query':'query { root { field } }'}".replace("'", "\"");
        final Map<String, Map<String, Map<String, Object>>> response = (Map)
                endpoint.execute(request, new GraphQLContext());
        assertEquals("value", response.get("data").get("root").get("field"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testExecuteMutation() throws Exception {
        final Provider provider = new Provider();
        final GraphQLRestServer endpoint =
                GraphQLRestServer.builder()
                .providers(Lists.newArrayList(new GraphQLProviders("root", provider)))
                .build();

        final String request = (
                "{'query':'mutation { " +
                "  root { " +
                "    updateField(argument: { " +
                "      nested: { " +
                "        inputField: \\'value\\' " +
                "      } " +
                "    }) " +
                "  } " +
                "}'}").replace("'", "\"");
        final Map<String, Map<String, Map<String, Object>>> response = (Map)
                endpoint.execute(request, new GraphQLContext());
        assertEquals("value", response.get("data").get("root").get("updateField"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testExecuteList() throws Exception {
        final Provider provider = new Provider();
        final GraphQLRestServer endpoint = GraphQLRestServer.builder().provider(new GraphQLProviders("root", provider)).build();

        final String request = "[ {'query':'query { root { field } }'} ]".replace("'", "\"");
        final List<Map<String, Map<String, Map<String, Object>>>> response = (List)
                endpoint.execute(request, new GraphQLContext());
        assertEquals("value", response.get(0).get("data").get("root").get("field"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testExecuteRequest() throws Exception {
        final Provider provider = new Provider();
        final GraphQLRestServer endpoint = GraphQLRestServer.builder().provider(new GraphQLProviders("root", provider)).build();

        final GraphQLRestRequest request = new GraphQLRestRequest("query { root { field } }");
        final Map<String, Map<String, Map<String, Object>>> response = (Map)
                endpoint.execute(request, new GraphQLContext());
        assertEquals("value", response.get("data").get("root").get("field"));
    }

    public static class Provider {
        @GraphQLName("field")
        public String field(@GraphQLName("argument") final String argument) {
            return argument != null ? argument : "value";
        }

        @GraphQLName("updateField")
        @GraphQLMutation
        public String updateField(@GraphQLName("argument") final InputMessage input) {
            return input.nested.inputField;
        }
    }

    public static class BaseInputMessage {
        @GraphQLName
        NestedInputField nested;
    }

    public static class InputMessage extends BaseInputMessage {
    }

    public static class NestedInputField {
        @GraphQLName
        String inputField;
    }
}
