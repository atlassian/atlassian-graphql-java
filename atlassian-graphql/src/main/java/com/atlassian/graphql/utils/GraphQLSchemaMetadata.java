package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.expansions.GraphQLExpandable;
import com.atlassian.graphql.spi.GraphQLExtensions;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;

import static java.util.Objects.requireNonNull;

/**
 * Provides meta-data for graph-ql type objects.
 */
public class GraphQLSchemaMetadata {
    private static final WeakHashMap<GraphQLFieldDefinition, String> expansionFields = new WeakHashMap<>();
    private static final WeakHashMap<GraphQLFieldDefinition, Object> skippedFields = new WeakHashMap<>();

    /**
     * Get whether a graph-ql field is an expansion field.
     * An expansion field is a field that may be specified in an 'expand' REST API query parameter.
     */
    public static boolean isExpansionField(final GraphQLFieldDefinition fieldDefinition) {
        requireNonNull(fieldDefinition);
        return expansionFields.containsKey(fieldDefinition);
    }

    /**
     * Get the override expansion path of a graph-ql field, if it has one.
     */
    public static String getExpansionFieldOverride(final GraphQLFieldDefinition fieldDefinition) {
        return expansionFields.get(fieldDefinition);
    }

    /**
     * Get whether the field should be skipped in expansion path calculations
     */
    public static boolean isSkippedField(final GraphQLFieldDefinition fieldDefinition) {
        requireNonNull(fieldDefinition);
        return skippedFields.containsKey(fieldDefinition);
    }

    /**
     * Associate the meta-data from a java object field to the graph-ql field definition.
     */
    public static void applyFieldMetadata(
            final Member accessor,
            final GraphQLFieldDefinition graphQLField,
            final GraphQLExtensions extensions) {

        requireNonNull(accessor);
        requireNonNull(graphQLField);

        // if the field is expandable (@GraphQLExpandable, or in Confluence a Reference, Map or List)
        markIfExpansionField(accessor, extensions, graphQLField);
    }

    /**
     * Mark all fields on a type as requiring an entry in the 'expand' parameter.
     * @param type The type
     * @param allTypes All the types that have been built
     * @param recursive True to also mark nested fields as expandable
     */
    public static void markAllFieldsExpandable(
            GraphQLOutputType type,
            final Map<String, GraphQLType> allTypes,
            final boolean recursive) {

        requireNonNull(type);
        requireNonNull(allTypes);

        if (type instanceof GraphQLTypeReference) {
            return;
        }
        type = (GraphQLOutputType) GraphQLUtils.unwrap(type, allTypes);
        if (!(type instanceof GraphQLObjectType)) {
            return;
        }

        final GraphQLObjectType objectType = (GraphQLObjectType) type;
        for (final GraphQLFieldDefinition fieldDefinition : objectType.getFieldDefinitions()) {
            if (!isExpansionField(fieldDefinition)) {
                markIsExpansionField(fieldDefinition);
                if (recursive) {
                    markAllFieldsExpandable(fieldDefinition.getType(), allTypes, true);
                }
            }
        }
    }

    public static void markIsExpansionField(final GraphQLFieldDefinition graphQLField) {
        requireNonNull(graphQLField);
        expansionFields.put(graphQLField, null);
    }

    private static void markIfExpansionField(final Member accessor, final GraphQLExtensions extensions,
                                             GraphQLFieldDefinition graphQLField) {
        requireNonNull(graphQLField);
        if (extensions != null && extensions.isExpansionField(accessor)) {
            expansionFields.put(graphQLField, null);
            return;
        }

        GraphQLExpandable expandable = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, GraphQLExpandable.class);
        if (expandable == null) {
            if (accessor instanceof Field) {
                expandable = AnnotationsHelper.getAnnotation(((Field) accessor).getType(), GraphQLExpandable.class);
            } else if (accessor instanceof Method) {
                expandable = AnnotationsHelper.getAnnotation(((Method) accessor).getReturnType(), GraphQLExpandable.class);
            }
        }
        if (expandable != null) {
            if (!Objects.equals(expandable.value(), "")) {
                expansionFields.put(graphQLField, expandable.value());
            } else {
                expansionFields.put(graphQLField, null);
            }
            if (expandable.skip()) {
                skippedFields.put(graphQLField, null);
            }
        }
    }


    private static final HashSet<GraphQLFieldDefinition> experimentalFields = new HashSet<>();
    private static final HashSet<GraphQLType> experimentalTypes = new HashSet<>();

    /**
     * Get whether a graph-ql field is an experimental field.
     */
    public static boolean isExperimentalField(final GraphQLFieldDefinition fieldDefinition) {
        requireNonNull(fieldDefinition);
        return experimentalFields.contains(fieldDefinition);
    }

    /**
     * Mark a graph-ql field as an experimental field.
     */
    public static void markFieldExperimental(final GraphQLFieldDefinition fieldDefinition) {
        requireNonNull(fieldDefinition);
        experimentalFields.add(fieldDefinition);
    }

    /**
     * Get whether a graph-ql field is an experimental field.
     */
    public static boolean isExperimentalType(final GraphQLType graphQLType) {
        requireNonNull(graphQLType);
        return experimentalTypes.contains(graphQLType);
    }

    /**
     * Mark a graph-ql field as an experimental field.
     */
    public static void markTypeExperimental(final GraphQLType graphQLType) {
        requireNonNull(graphQLType);
        experimentalTypes.add(graphQLType);
    }

}
