package com.atlassian.graphql.test.json.spi;

import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * The result of the GraphQLTestTypeBuilder.buildType method.
 */
public class GraphQLTestTypeBuilderResult {
    private final GraphQLType type;
    private final Map<String, GraphQLType> allTypes;

    public GraphQLTestTypeBuilderResult(final GraphQLType type, final Map<String, GraphQLType> allTypes) {
        this.type = requireNonNull(type);
        this.allTypes = requireNonNull(allTypes);
    }

    /**
     * Get the type that was built.
     */
    public GraphQLType getType() {
        return type;
    }

    /**
     * Get other types that were built, which are used to resolve {@link GraphQLTypeReference} type references.
     */
    public Map<String, GraphQLType> getAllTypes() {
        return allTypes;
    }
}
