package com.atlassian.graphql.provider.internal;

import com.atlassian.graphql.annotations.GraphQLMutation;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLOperationType;
import com.atlassian.graphql.annotations.GraphQLTypeName;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.ObjectTypeBuilderHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * A builder for {@link ProviderFieldTree} objects from a set of annotated provider objects and
 * their methods.
 */
public class ProviderFieldTreeBuilder {
    /**
     * Build the field tree.
     */
    public ProviderFieldTree build(
            final List<GraphQLProviders> providers,
            final String rootTypeName,
            final GraphQLOperationType operationType) {

        requireNonNull(providers);
        requireNonNull(rootTypeName);
        requireNonNull(operationType);

        final ProviderFieldTree fieldTree = new ProviderFieldTree("root", rootTypeName);
        for (final GraphQLProviders pathAndProvider : providers) {
            addProvidersToTree(fieldTree, pathAndProvider, operationType);
        }
        return fieldTree;
    }

    private void addProvidersToTree(
            final ProviderFieldTree fieldTree,
            final GraphQLProviders pathAndProviders,
            final GraphQLOperationType operationType) {

        for (final Object provider : pathAndProviders.getProviders()) {
            final GraphQLName providerFieldPath = AnnotationsHelper.getAnnotation(provider.getClass(), GraphQLName.class);
            final List<String> providerPath = getFieldPath(pathAndProviders.getPath(), providerFieldPath);

            for (final Method method : ObjectTypeBuilderHelper.getMethodsInOrder(provider.getClass())) {
                final GraphQLName methodFieldPath = AnnotationsHelper.getAnnotation(method, GraphQLName.class);
                if (methodFieldPath != null) {
                    final boolean isMutation = AnnotationsHelper.hasAnnotation(method, GraphQLMutation.class);
                    if (isMutation != (operationType == GraphQLOperationType.MUTATION)) {
                        continue;
                    }
                    final List<String> methodPath = getFieldPath(methodFieldPath.value());
                    addToTree(fieldTree, provider, method, providerPath, methodPath);
                }
            }
        }
    }

    private static void addToTree(
            ProviderFieldTree fieldTree,
            final Object provider,
            final Method providerMethod,
            final List<String> providerPath,
            final List<String> methodPath) {

        fieldTree = getOrCreateProviderField(fieldTree, provider, providerPath);
        fieldTree = getOrCreateMethodField(fieldTree, methodPath);

        if (fieldTree.getProvider() != null) {
            throw new IllegalArgumentException("Field '" + fieldTree.getFieldName() + "' cannot be bound to multiple provider objects");
        }
        fieldTree.setProvider(provider);
        fieldTree.setProviderMethod(providerMethod);
    }

    /**
     * Get (or create) a provider field in the hierarchy.
     */
    private static ProviderFieldTree getOrCreateProviderField(
            ProviderFieldTree fieldTree,
            final Object provider,
            final List<String> providerPath) {

        String currentTypeName = fieldTree.getTypeName();
        for (int i = 0; i < providerPath.size(); i++) {
            final String fieldName = providerPath.get(i);
            currentTypeName += "_" + fieldName;

            // apply @GraphQLTypeName for the provider type
            if (isLast(providerPath, i)) {
                final String providerTypeName = findProviderTypeName(provider);
                currentTypeName = providerTypeName != null ? providerTypeName : currentTypeName;
            }
            fieldTree = fieldTree.getOrCreateChildField(fieldName, currentTypeName);
        }
        return fieldTree;
    }

    private static ProviderFieldTree getOrCreateMethodField(
            ProviderFieldTree fieldTree,
            final List<String> methodPath) {

        String currentTypeName = fieldTree.getTypeName();
        for (int i = 0; i < methodPath.size(); i++) {
            final String fieldName = methodPath.get(i);
            currentTypeName += "_" + fieldName;

            // the last field type name will be filled in by the method return type
            final String typeName = isLast(methodPath, i) ? null : currentTypeName;
            fieldTree = fieldTree.getOrCreateChildField(fieldName, typeName);
        }
        return fieldTree;
    }

    private static String findProviderTypeName(final Object provider) {
        return provider != null ? findProviderTypeName(provider.getClass()) : null;
    }

    private static String findProviderTypeName(final Class providerType) {
        if (providerType == null || providerType == Object.class) {
            return null;
        }
        final GraphQLTypeName typeName = AnnotationsHelper.getAnnotation(providerType, GraphQLTypeName.class);
        if (typeName != null) {
            return typeName.value();
        }
        return findProviderTypeName(providerType.getSuperclass());
    }

    private static List<String> getFieldPath(
            final String rootPath,
            final GraphQLName path) {

        final List<String> list = new ArrayList<>();
        list.addAll(getFieldPath(rootPath));
        if (path != null) {
            list.addAll(getFieldPath(path));
        }
        return list;
    }

    private static List<String> getFieldPath(final GraphQLName fieldPath) {
        return getFieldPath(fieldPath.value());
    }

    private static List<String> getFieldPath(final String path) {
        return !Strings.isNullOrEmpty(path)
                ? Lists.newArrayList((path.startsWith("/") ? path.substring(1) : path).split("/"))
                : Collections.emptyList();
    }

    private static <T> boolean isLast(final List<T> list, final int i) {
        return i == list.size() - 1;
    }
}
