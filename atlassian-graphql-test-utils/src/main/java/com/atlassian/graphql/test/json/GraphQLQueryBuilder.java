package com.atlassian.graphql.test.json;

import com.google.common.collect.Lists;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLFieldsContainer;
import graphql.schema.GraphQLInterfaceType;
import graphql.schema.GraphQLNamedOutputType;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.graphql.test.utils.GraphQLUtils.unwrap;
import static java.util.Objects.requireNonNull;

/**
 * Generates graphql query strings by traversing the field names
 * from {@link GraphQLOutputType} objects.
 */
public class GraphQLQueryBuilder {
    /**
     * Build a graphql query string that projects all possible fields (excluding circular references)
     * from a {@link GraphQLOutputType}.
     * @param type The graphql type from which to reflect the field tree
     * @param allTypes All graphql types generated, which is required to resolve {@link GraphQLTypeReference}
     * @param expansions A list of field expansion paths to use when generating the query,
     *                   or null to generate the entire tree
     * @return The generated query string
     */

    public static String build(
            final GraphQLOutputType type,
            final Map<String, GraphQLType> allTypes,
            final List<String> expansions) {
        return build(type, allTypes, expansions, "  ");
    }

    /**
     * Build a graphql query string that projects all possible fields (excluding circular references)
     * from a {@link GraphQLOutputType}.
     * @param type The graphql type from which to reflect the field tree
     * @param allTypes All graphql types generated, which is required to resolve {@link GraphQLTypeReference}
     * @param expansions A list of field expansion paths to use when generating the query,
     *                   or null to generate the entire tree
     * @param indentUnit The string for each indent level.
     * @return The generated query string
     */
    public static String build(
            final GraphQLOutputType type,
            final Map<String, GraphQLType> allTypes,
            final List<String> expansions,
            final String indentUnit) {

        requireNonNull(type);
        requireNonNull(allTypes);

        final StringBuilder query = new StringBuilder();
        build(query, indentUnit, indentUnit, (GraphQLOutputType) unwrap(type, allTypes), allTypes, expansions, Lists.newArrayList());
        return "{\n" + query + "}";
    }

    private static void build(
            final StringBuilder query,
            final String indent,
            final String indentUnit,
            GraphQLOutputType type,
            final Map<String, GraphQLType> allTypes,
            final List<String> expansions,
            final List<String> typesInPath) {

        if (!(type instanceof GraphQLFieldsContainer)) {
            return;
        }
        final GraphQLFieldsContainer fieldContainer = (GraphQLFieldsContainer) type;
        build(query, indent, indentUnit, fieldContainer, fieldContainer.getFieldDefinitions(), allTypes, expansions, typesInPath);
    }

    private static void build(
            final StringBuilder query,
            final String indent,
            final String indentUnit,
            final GraphQLFieldsContainer type,
            final List<GraphQLFieldDefinition> fields,
            final Map<String, GraphQLType> allTypes,
            final List<String> expansions,
            final List<String> typesInPath) {

        for (final GraphQLFieldDefinition field : fields) {
            final GraphQLOutputType fieldType = (GraphQLOutputType) unwrap(field.getType(), allTypes);
            if (shouldTraverseField(field.getName(), fieldType, typesInPath, expansions)) {
                final List<String> nextExpansions = traverseExpansions(expansions, field.getName());
                buildField(query, indent, indentUnit, allTypes, field.getName(), fieldType, nextExpansions, typesInPath);
            }
        }
        if (type instanceof GraphQLInterfaceType) {
            buildInterfaceInlineFragments(query, indent, indentUnit, (GraphQLInterfaceType) type, allTypes, expansions, typesInPath);
        }
    }

    private static void buildInterfaceInlineFragments(
            final StringBuilder query,
            final String indent,
            final String indentUnit,
            final GraphQLInterfaceType interfaceType,
            final Map<String, GraphQLType> allTypes,
            final List<String> expansions,
            final List<String> typesInPath) {

        for (final GraphQLType type : allTypes.values()) {
            if (!(type instanceof GraphQLObjectType)) {
                continue;
            }
            final GraphQLObjectType objectType = (GraphQLObjectType) type;
            final List<GraphQLNamedOutputType> interfaces = objectType.getInterfaces();
            if (interfaces.stream().anyMatch(x -> x.getName().equals(interfaceType.getName()))) {
                buildInterfaceInlineFragment(query, indent, indentUnit, interfaceType, objectType, allTypes, expansions, typesInPath);
            }
        }
    }

    private static void buildInterfaceInlineFragment(
            final StringBuilder query,
            final String indent,
            final String indentUnit,
            final GraphQLInterfaceType interfaceType,
            final GraphQLObjectType objectType,
            final Map<String, GraphQLType> allTypes,
            final List<String> expansions,
            final List<String> typesInPath) {

        final List<GraphQLFieldDefinition> fields = excludeInterfaceFields(interfaceType, objectType);
        if (!fields.isEmpty()) {
            query.append(indent).append("... on ").append(objectType.getName()).append(" {\n");
            build(query, indent + indentUnit, indentUnit, objectType, fields, allTypes, expansions, typesInPath);
            query.append(indent).append("}\n");
        }
    }

    private static List<GraphQLFieldDefinition> excludeInterfaceFields(
            final GraphQLInterfaceType interfaceType,
            final GraphQLObjectType objectType) {

        return objectType.getFieldDefinitions().stream()
               .filter(concreteField -> !isInterfaceField(interfaceType, concreteField))
               .collect(Collectors.toList());
    }

    private static boolean isInterfaceField(
            final GraphQLInterfaceType interfaceType,
            final GraphQLFieldDefinition concreteField) {

        return interfaceType.getFieldDefinitions().stream().anyMatch(
                interfaceField -> interfaceField.getName().equals(concreteField.getName()));
    }

    private static boolean shouldTraverseField(
            final String fieldName,
            final GraphQLOutputType fieldType,
            final List<String> typesInPath,
            final List<String> expansions) {

        // always render primitive fields
        if (!(fieldType instanceof GraphQLFieldsContainer)) {
            return true;
        }

        // fall out if we're just repeating the graph infinitely
        if (expansions == null) {
            return !isRepeatingPath(typesInPath);
        }

        // if there are expansions, traverse them as specified
        for (final String expand : expansions) {
            if (fieldName.equals(expand) || expand.startsWith(fieldName + ".")) {
                return true;
            }
        }
        return false;
    }

    private static boolean isRepeatingPath(List<String> typesInPath) {
        if (typesInPath.isEmpty()) {
            return false;
        }
        final String last = typesInPath.get(typesInPath.size() - 1);
        return typesInPath.stream().filter(type -> type.equals(last)).count() >= 2;
    }

    private static List<String> traverseExpansions(final List<String> expansions, final String fieldName) {
        if (expansions == null) {
            return null;
        }
        final List<String> result = Lists.newArrayList();
        for (final String expand : expansions) {
            if (expand.startsWith(fieldName + ".")) {
                result.add(expand.substring(fieldName.length() + 1));
            }
        }
        return result;
    }

    private static void buildField(
            final StringBuilder query,
            final String indent,
            final String indentUnit,
            final Map<String, GraphQLType> allTypes,
            final String fieldName,
            final GraphQLOutputType fieldType,
            final List<String> expansions,
            final List<String> typesInPath) {

        query.append(indent).append(fieldName);
        if (fieldType instanceof GraphQLFieldsContainer) {
            query.append(" {").append("\n");

            typesInPath.add(((GraphQLFieldsContainer) fieldType).getName());
            build(query, indent + indentUnit, indentUnit, fieldType, allTypes, expansions, typesInPath);
            typesInPath.remove(typesInPath.size() - 1);

            query.append(indent).append("}");
        }
        query.append("\n");
    }
}
