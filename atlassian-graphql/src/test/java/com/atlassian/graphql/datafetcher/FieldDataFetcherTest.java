package com.atlassian.graphql.datafetcher;

import com.google.common.collect.ImmutableMap;
import graphql.schema.DataFetchingEnvironment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FieldDataFetcherTest {
    @Mock
    private DataFetchingEnvironment dataFetchingEnvironment;

    @Test
    public void testGetObjectField() throws Exception {
        when(dataFetchingEnvironment.getSource()).thenReturn(new TestObject("value"));
        final Field field = TestObject.class.getDeclaredField("field");
        field.setAccessible(true);
        final FieldDataFetcher dataFetcher = new FieldDataFetcher("field", field);
        assertEquals("value", dataFetcher.get(dataFetchingEnvironment));
    }

    @Test
    public void testGetFromMap() throws Exception {
        final Map<String, Object> map = ImmutableMap.of("field", "value");
        when(dataFetchingEnvironment.getSource()).thenReturn(map);
        final FieldDataFetcher dataFetcher = new FieldDataFetcher("field", null);
        assertEquals("value", dataFetcher.get(dataFetchingEnvironment));
    }

    @Test
    public void testGetFromUnknownTypeAndNotFound() throws Exception {
        when(dataFetchingEnvironment.getSource()).thenReturn(new Object());
        final FieldDataFetcher dataFetcher = new FieldDataFetcher("field", null);
        assertNull(dataFetcher.get(dataFetchingEnvironment));
    }

    public static class TestObject {
        private String field;

        public TestObject(final String field) {
            this.field = field;
        }
    }
}
