package com.atlassian.graphql.schema;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.types.RootGraphQLTypeBuilder;
import graphql.Scalars;
import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLEnumType;
import graphql.schema.GraphQLEnumValueDefinition;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;
import org.junit.Test;

import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class GraphQLTypeSchemaPrinterTest {
    @Test
    public void testScalars() {
        GraphQLObjectType type =
                GraphQLObjectType.newObject()
                        .name("typeName")
                        .field(field("intField", GraphQLInt))
                        .field(field("longField", ExtendedScalars.GraphQLLong))
                        .field(field("shortField", ExtendedScalars.GraphQLShort))
                        .field(field("byteField", ExtendedScalars.GraphQLByte))
                        .field(field("floatField", Scalars.GraphQLFloat))
                        .field(field("bigIntegerField", ExtendedScalars.GraphQLBigInteger))
                        .field(field("bigDecimalField", ExtendedScalars.GraphQLBigDecimal))
                        .field(field("stringField", Scalars.GraphQLString))
                        .field(field("booleanField", Scalars.GraphQLBoolean))
                        .field(field("idField", Scalars.GraphQLID))
                        .field(field("charField", ExtendedScalars.GraphQLChar))
                        .build();
        assertEquals(
                "type typeName {\n" +
                        "  intField: Int\n" +
                        "  longField: Int\n" +
                        "  shortField: Int\n" +
                        "  byteField: Int\n" +
                        "  floatField: Float\n" +
                        "  bigIntegerField: Int\n" +
                        "  bigDecimalField: Float\n" +
                        "  stringField: String\n" +
                        "  booleanField: Boolean\n" +
                        "  idField: ID\n" +
                        "  charField: String\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(type));
    }

    @Test
    public void testEnumAsArgument() {
        GraphQLEnumType enumType = GraphQLEnumType.newEnum().name("MyType")
                .value(GraphQLEnumValueDefinition.newEnumValueDefinition()
                        .name("foo")
                        .description("")
                        .value("bar")
                .build())
                .value(GraphQLEnumValueDefinition.newEnumValueDefinition()
                        .name("bar")
                        .description("")
                        .value("bar").build()).build();
        GraphQLObjectType type =
                GraphQLObjectType.newObject()
                        .name("typeName")
                        .field(newFieldDefinition()
                                .name("stringField")
                                .type(Scalars.GraphQLString)
                                .argument(GraphQLArgument.newArgument()
                                        .name("type")
                                        .type(new GraphQLTypeReference("MyType"))
                                        .build())
                                .build())
                        .build();
        assertEquals(
                "type typeName {\n" +
                        "  stringField(type: MyType): String\n" +
                        "}\n" +
                        "\n" +
                        "enum MyType {\n" +
                        "  foo\n" +
                        "  bar\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(type, asList(enumType, type)));
    }

    @Test
    public void testDefaultValues() {
        GraphQLObjectType type =
                GraphQLObjectType.newObject()
                        .name("typeName")
                        .field(newFieldDefinition()
                                        .name("intField")
                                        .type(GraphQLInt)
                                        .argument(GraphQLArgument.newArgument()
                                                .name("id")
                                                .type(GraphQLInt)
                                                .defaultValueProgrammatic(42)
                                                .build())
                                        .build())
                        .field(newFieldDefinition()
                                        .name("strField")
                                        .type(GraphQLString)
                                        .argument(GraphQLArgument.newArgument()
                                                .name("str")
                                                .type(GraphQLString)
                                                .defaultValueProgrammatic("blah")
                                                .build())
                                        .build())
                        .build();
        assertEquals(
                "type typeName {\n" +
                        "  intField(id: Int = 42): Int\n" +
                        "  strField(str: String = \"blah\"): String\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(type).trim());
    }

    @Test
    public void testPrintRecursiveTypeOnce() {
        GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        GraphQLType type = new RootGraphQLTypeBuilder().buildType(Recursive.class, context);
        assertEquals(
                "type Recursive {\n" +
                        "  self: Recursive\n" +
                        "}", new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(type, context.getTypes()));
    }

    private static GraphQLFieldDefinition field(final String name, final GraphQLOutputType type) {
        return newFieldDefinition().name(name).type(type).build();
    }

    public static class Recursive {
        @GraphQLName
        private Recursive self;
    }
}
