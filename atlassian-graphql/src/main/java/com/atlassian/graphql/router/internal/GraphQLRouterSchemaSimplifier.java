package com.atlassian.graphql.router.internal;

import com.atlassian.graphql.router.GraphQLRouteRules;
import com.atlassian.graphql.router.GraphQLRouter;
import com.google.common.base.Strings;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Removing unused schema types and fields on behalf of {@link GraphQLRouter}.
 */
public class GraphQLRouterSchemaSimplifier {
    @SuppressWarnings("unchecked")
    public void trimSchema(final GraphQLRouteRules fieldRules, final Map<String, Object> schema) {
        if (schema == null) {
            return;
        }
        final String queryTypeName = getQueryTypeName(schema);
        if (queryTypeName == null) {
            return;
        }
        final List<Map<String, Object>> typesList = (List<Map<String, Object>>) schema.get("types");
        if (typesList == null) {
            return;
        }
        final LinkedHashMap<String, Map<String, Object>> types = new LinkedHashMap<>();
        for (final Map<String, Object> type : typesList) {
            types.put((String) type.get("name"), type);
        }
        final Map<String, Object> queryType = types.get(queryTypeName);
        if (queryType == null) {
            return;
        }

        // find which types we need to keep
        final Set<String> typeNamesToKeep = new HashSet<>();
        final List<Map<String, Object>> fields = (List<Map<String, Object>>) queryType.get("fields");
        for (final Map<String, Object> field : fields) {
            final String fieldName = (String) field.get("name");
            if (fieldRules.matchesFieldName(fieldName)) {
                visitField(field, typeNamesToKeep, types);
            }
        }

        // keep system types
        for (final Map<String, Object> type : typesList) {
            final String typeName = (String) type.get("name");
            if (typeName.startsWith("__")) {
                visitType(type, typeNamesToKeep, types);
            }
        }

        // for the root query type, remove unused fields
        typeNamesToKeep.add(queryTypeName);
        if (!Strings.isNullOrEmpty(fieldRules.getRootFieldName())) {
            final List<Map<String, Object>> queryTypeFields = (List<Map<String, Object>>) queryType.get("fields");
            queryTypeFields.removeIf(field -> !fieldRules.matchesFieldName((String) field.get("name")));
        }

        // remove used types (not visited above)
        typesList.removeIf(type -> !typeNamesToKeep.contains((String) type.get("name")));
    }

    @SuppressWarnings("unchecked")
    private static String getQueryTypeName(final Map<String, Object> schema) {
        final Map<String, Object> queryTypeNameMap = (Map<String, Object>) schema.get("queryType");
        return queryTypeNameMap != null ? (String) queryTypeNameMap.get("name") : null;
    }

    @SuppressWarnings("unchecked")
    private static void visitTypes(
            final List<Map<String, Object>> types,
            final Set<String> typeNames,
            final LinkedHashMap<String, Map<String, Object>> allTypes) {

        if (types == null) {
            return;
        }
        for (final Map<String, Object> type : types) {
            visitType(type, typeNames, allTypes);
        }
    }

    @SuppressWarnings("unchecked")
    private static void visitType(
            final Map<String, Object> type,
            final Set<String> typeNames,
            final LinkedHashMap<String, Map<String, Object>> allTypes) {

        if (type == null) {
            return;
        }

        final String typeName = (String) type.get("name");
        if (typeNames.contains(typeName)) {
            return;
        }
        if (typeName != null) {
            final Map<String, Object> realType = allTypes.get(typeName);
            if (realType != type) {
                visitType(realType, typeNames, allTypes);
            }
            typeNames.add(typeName);
        }

        final List<Map<String, Object>> fields = (List<Map<String, Object>>) type.get("fields");
        if (fields != null) {
            for (final Map<String, Object> field : fields) {
                visitField(field, typeNames, allTypes);
            }
        }

        final Map<String, Object> ofType = (Map<String, Object>) type.get("ofType");
        visitType(ofType, typeNames, allTypes);

        final List<Map<String, Object>> interfaces = (List<Map<String, Object>>) type.get("interfaces");
        visitTypes(interfaces, typeNames, allTypes);

        final List<Map<String, Object>> possibleTypes = (List<Map<String, Object>>) type.get("possibleTypes");
        visitTypes(possibleTypes, typeNames, allTypes);
    }

    @SuppressWarnings("unchecked")
    private static void visitField(
            final Map<String, Object> field,
            final Set<String> typeNames,
            final LinkedHashMap<String, Map<String, Object>> allTypes) {

        final Map<String, Object> fieldType = (Map<String, Object>) field.get("type");
        if (fieldType != null) {
            visitType(fieldType, typeNames, allTypes);
        }

        final List<Map<String, Object>> args = (List<Map<String, Object>>) field.get("args");
        if (args != null) {
            for (final Map<String, Object> arg : args) {
                final Map<String, Object> argType = (Map<String, Object>) arg.get("type");
                if (argType != null) {
                    visitType(fieldType, typeNames, allTypes);
                }
            }
        }
    }
}
