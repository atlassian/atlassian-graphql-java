package com.atlassian.graphql.test.json;

import com.atlassian.graphql.annotations.GraphQLInterface;
import com.atlassian.graphql.json.types.JsonRootGraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLOutputType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GraphQLQueryBuilderTest {
    @Test
    public void testBuildEntireTree() {
        final GraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLOutputType type = (GraphQLOutputType) typeBuilder.buildType(TestObject.class, context);

        final String query = GraphQLQueryBuilder.build(type, context.getTypes(), null);
        assertEquals(
                "{\n" +
                        "  property\n" +
                        "  nestedObject {\n" +
                        "    nestedProperty\n" +
                        "  }\n" +
                        "}", query);
    }

    @Test
    public void testBuildEntireTreeNoIndent() {
        final GraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLOutputType type = (GraphQLOutputType) typeBuilder.buildType(TestObject.class, context);

        final String query = GraphQLQueryBuilder.build(type, context.getTypes(), null, "");
        assertEquals(
                "{\n" +
                        "property\n" +
                        "nestedObject {\n" +
                        "nestedProperty\n" +
                        "}\n" +
                        "}", query);
    }

    @Test
    public void testBuildSpecificPath() {
        final GraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLOutputType type = (GraphQLOutputType) typeBuilder.buildType(TestObject.class, context);

        String query = GraphQLQueryBuilder.build(type, context.getTypes(), Lists.newArrayList(""));
        assertEquals(
                "{\n" +
                "  property\n" +
                "}", query);

        query = GraphQLQueryBuilder.build(type, context.getTypes(), Lists.newArrayList("nestedObject"));
        assertEquals(
                "{\n" +
                "  property\n" +
                "  nestedObject {\n" +
                "    nestedProperty\n" +
                "  }\n" +
                "}", query);
    }

    @Test
    public void testBuildInterfaceInlineFragments() {
        final GraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLOutputType type = (GraphQLOutputType) typeBuilder.buildType(TestInterface.class, context);

        final String query = GraphQLQueryBuilder.build(type, context.getTypes(), Lists.newArrayList(""));
        assertEquals("" +
                "{\n" +
                "  interfaceField\n" +
                "  ... on TestConcrete {\n" +
                "    concreteField\n" +
                "  }\n" +
                "}", query);
    }

    public static class TestObject {
        @JsonProperty
        private String property;

        @JsonProperty
        private TestNestedObject nestedObject;
    }

    public static class TestNestedObject {
        @JsonProperty
        private String nestedProperty;
    }

    @GraphQLInterface(possibleTypes = TestConcrete.class)
    public static class TestInterface {
        @JsonProperty
        private String interfaceField;
    }

    public static class TestConcrete extends TestInterface {
        @JsonProperty
        private String concreteField;
    }
}
