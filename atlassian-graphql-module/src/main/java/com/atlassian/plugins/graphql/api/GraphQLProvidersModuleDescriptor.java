package com.atlassian.plugins.graphql.api;

import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.spi.CombinedGraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.spi.GraphQLTypeContributor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.util.validation.ValidationPattern;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleWiring;

import javax.annotation.Nonnull;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;
import static java.util.stream.StreamSupport.stream;
import static org.osgi.framework.wiring.BundleWiring.LISTRESOURCES_LOCAL;
import static org.osgi.framework.wiring.BundleWiring.LISTRESOURCES_RECURSE;

/**
 * The {@link ModuleDescriptor} that provides the graph-ql providers for a plugin.
 */
public class GraphQLProvidersModuleDescriptor extends AbstractModuleDescriptor<GraphQLProviders> {
    private static Pattern RESOURCE_SUFFIX_REGEX = Pattern.compile("\\.class$");

    private GraphQLProviders graphQLProviders;
    private String path;
    private List<Class> providerClasses;
    private OsgiPlugin osgiPlugin;

    public GraphQLProvidersModuleDescriptor(@ComponentImport final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public Class<GraphQLProviders> getModuleClass() {
        return GraphQLProviders.class;
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException {
        super.init(plugin, element);
        osgiPlugin = (OsgiPlugin) plugin;

        final Set<String> packageNames = parsePackages(element);
        path = element.attributeValue("path");
        providerClasses = loadProviderClasses(packageNames, osgiPlugin);
    }

    @Override
    protected void provideValidationRules(final ValidationPattern pattern) {
        pattern.rule(test("package").withError("At least one 'package' element is required"));
    }

    @Override
    public GraphQLProviders getModule() {
        if (graphQLProviders == null) {
            final List<Object> providers = createProviderObjects(GraphQLProvider.class);
            final List<Object> extensions = createProviderObjects(com.atlassian.graphql.annotations.GraphQLExtensions.class);
            graphQLProviders = new GraphQLProviders(path, providers, toGraphQLExtensions(extensions));
        }
        return graphQLProviders;
    }

    private <T extends Annotation> List<Object> createProviderObjects(final Class<T> annotationType) {
        return (List<Object>)
                providerClasses.stream()
                .filter(providerClass -> hasAnnotation(providerClass, annotationType))
                .map(providerClass -> osgiPlugin.getContainerAccessor().createBean(providerClass))
                .collect(Collectors.toList());
    }

    private static GraphQLExtensions toGraphQLExtensions(final List<Object> extensions) {
        final List<GraphQLExtensions> list = new ArrayList<>();
        for (final Object obj : extensions) {
            if (obj instanceof GraphQLExtensions) {
                list.add((GraphQLExtensions) obj);
            } else if (obj instanceof GraphQLTypeContributor) {
                list.add(GraphQLExtensions.of((GraphQLTypeContributor) obj));
            } else {
                throw new RuntimeException(String.format(
                        "Class '%s' must implement interface %s or %s",
                        obj.getClass().getName(),
                        GraphQLTypeContributor.class.getName(),
                        GraphQLExtensions.class.getName()));
            }
        }
        return CombinedGraphQLExtensions.combine(list);
    }

    private List<Class> loadProviderClasses(final Iterable<String> packageNames, final OsgiPlugin osgiPlugin) {
        final List<Class> list = new ArrayList<>();
        final BundleWiring bundleWiring = osgiPlugin.getBundle().adapt(BundleWiring.class);
        final Collection<String> bundleResourceNames = bundleWiring.listResources("/", "*.class", LISTRESOURCES_LOCAL | LISTRESOURCES_RECURSE);

        for (final String packageName : packageNames) {
            final List<Class> classesInPackage = loadClassesInPackage(
                    packageName, osgiPlugin.getBundle(), bundleResourceNames);

            for (final Class providerClass : classesInPackage) {
                if (isGraphQLProviderClass(providerClass)) {
                    list.add(providerClass);
                }
            }
        }
        return list;
    }

    private static List<Class> loadClassesInPackage(
            final String packageName,
            final Bundle pluginBundle,
            final Iterable<String> bundleResourceNames) {

        final String packagePrefix = getResourceNamePrefixForPackage(packageName);
        return stream(bundleResourceNames.spliterator(), false)
                .filter(name -> name.startsWith(packagePrefix))
                .sorted()
                .map(className -> loadClass(pluginBundle, className))
                .collect(Collectors.toList());
    }

    private static Class loadClass(final Bundle pluginBundle, final String providerName)
            throws PluginParseException {

        try {
            final String className =
                    RESOURCE_SUFFIX_REGEX.matcher(providerName)
                    .replaceAll("")
                    .replace('/', '.');
            return pluginBundle.loadClass(className);
        } catch (ClassNotFoundException ex) {
            throw new PluginParseException(ex);
        }
    }

    private boolean isGraphQLProviderClass(final Class providerClass) {
        return hasAnnotation(providerClass, GraphQLProvider.class)
               || hasAnnotation(providerClass, com.atlassian.graphql.annotations.GraphQLExtensions.class);
    }

    private Set<String> parsePackages(final Element element) {
        final List<Element> packageElements = (List<Element>) element.elements("package");
        return packageElements.stream().map(Element::getTextTrim).collect(Collectors.toSet());
    }

    private static String getResourceNamePrefixForPackage(final String packageName) {
        final String providerClassNamePrefix = StringUtils.replaceChars(packageName, '.', '/');

        // make sure we have a trailing / to not confuse packages such as com.mycompany.package
        // and com.mycompany.package1 which would both start with com/mycompany/package once transformed
        return !providerClassNamePrefix.endsWith("/")
               ? providerClassNamePrefix + '/'
               : providerClassNamePrefix;
    }

    private static <T extends Annotation> boolean hasAnnotation(
            final AnnotatedElement annotated,
            final Class<T> annotationClass) {

        return Arrays.stream(annotated.getAnnotations())
                .anyMatch(annotation -> annotation.annotationType().getName().equals(annotationClass.getName()));
    }
}
