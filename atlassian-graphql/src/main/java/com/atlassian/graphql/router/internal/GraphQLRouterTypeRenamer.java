package com.atlassian.graphql.router.internal;

import com.atlassian.graphql.router.GraphQLRouteTypeNameStrategy;
import com.atlassian.graphql.router.GraphQLRouter;
import com.atlassian.graphql.utils.GraphQLQueryVisitor;
import graphql.language.AstTransformer;
import graphql.language.Document;
import graphql.language.FragmentDefinition;
import graphql.language.InlineFragment;
import graphql.language.ListType;
import graphql.language.Node;
import graphql.language.NodeVisitor;
import graphql.language.NodeVisitorStub;
import graphql.language.NonNullType;
import graphql.language.Type;
import graphql.language.TypeName;
import graphql.language.VariableDefinition;
import graphql.util.TraversalControl;
import graphql.util.TraverserContext;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static graphql.util.TreeTransformerUtil.changeNode;

/**
 * Renames types for query and schema results on behalf of {@link GraphQLRouter}.
 */
public class GraphQLRouterTypeRenamer {
    private static final String SCHEMA_FIELD = "__schema";
    private static final String TYPENAME_FIELD = "__typename";

    /**
     * Rename the types referenced in a GraphQL query.
     */
    public Document renameQueryTypes(final GraphQLRouteTypeNameStrategy typeNameStrategy, final Document query) {
        if (typeNameStrategy == null) {
            return query;
        }
        AstTransformer astTransformer = new AstTransformer();
        return (Document)astTransformer.transform(query, new NodeVisitorStub() {
            @Override
            public TraversalControl visitFragmentDefinition(FragmentDefinition node, TraverserContext<Node> context) {
                FragmentDefinition changedFragmentDefinition = node.transform(
                        builder -> builder.typeCondition(renameTypeName(typeNameStrategy, node.getTypeCondition()))
                );
                return changeNode(context, changedFragmentDefinition);
            }
            @Override
            public TraversalControl visitInlineFragment(InlineFragment node, TraverserContext<Node> context) {
                InlineFragment changedInlineFragment = node.transform(
                        builder -> builder.typeCondition(renameTypeName(typeNameStrategy, node.getTypeCondition()))
                );
                return changeNode(context, changedInlineFragment);
            }
            @Override
            public TraversalControl visitVariableDefinition(VariableDefinition node, TraverserContext<Node> context) {
                VariableDefinition changedVariableDefinition = node.transform(
                        builder -> builder.type(renameType(typeNameStrategy, node.getType()))
                );
                return changeNode(context, changedVariableDefinition);
            }
        });
    }

    private static Type renameType(final GraphQLRouteTypeNameStrategy typeNameStrategy, final Type type) {
        if (type == null) {
            return null;
        }
        if (type instanceof TypeName) {
            return renameTypeName(typeNameStrategy, (TypeName) type);
        }
        if (type instanceof NonNullType) {
            return new NonNullType(renameType(typeNameStrategy, ((NonNullType) type).getType()));
        }
        if (type instanceof ListType) {
            return new ListType(renameType(typeNameStrategy, ((ListType) type).getType()));
        }
        throw new IllegalArgumentException("Unexpected GraphQL type in query '" + type.getClass().getName() + "'");
    }

    private static TypeName renameTypeName(final GraphQLRouteTypeNameStrategy typeNameStrategy, final TypeName type) {
        return type != null
               ? new TypeName(typeNameStrategy.translateTypeNameClientToRoute(type.getName()))
               : null;
    }

    /**
     * Rename the types referenced from __schema and __typename fragments returned from a GraphQL query.
     */
    @SuppressWarnings("unchecked")
    public void renameDataResultTypeNames(
            final GraphQLRouteTypeNameStrategy typeNameStrategy,
            final Map<String, Object> data) {

        if (typeNameStrategy == null || data == null) {
            return;
        }
        renameRouteToClientTypeName(typeNameStrategy, data, TYPENAME_FIELD);

        final Map<String, Object> schema = (Map<String, Object>) data.get(SCHEMA_FIELD);
        if (schema != null) {
            renameSchemaResultTypeNames(typeNameStrategy, schema);
        }

        for (final Object value : data.values()) {
            if (value instanceof Map) {
                renameDataResultTypeNames(typeNameStrategy, (Map<String, Object>) value);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void renameSchemaResultTypeNames(
            final GraphQLRouteTypeNameStrategy typeNameStrategy,
            final Map<String, Object> schema) {

        renameSchemaTypeTypeNames(typeNameStrategy, (Map<String, Object>) schema.get("queryType"));
        renameSchemaTypeTypeNames(typeNameStrategy, (Map<String, Object>) schema.get("mutationType"));
        renameSchemaTypeTypeNames(typeNameStrategy, (Map<String, Object>) schema.get("subscriptionType"));

        final List<Map<String, Object>> types = (List<Map<String, Object>>) schema.get("types");
        renameSchemaTypeTypeNames(typeNameStrategy, types);
    }

    @SuppressWarnings("unchecked")
    private static void renameSchemaTypeTypeNames(
            final GraphQLRouteTypeNameStrategy typeNameStrategy,
            final List<Map<String, Object>> types) {

        if (types != null) {
            for (final Map<String, Object> type : types) {
                renameSchemaTypeTypeNames(typeNameStrategy, type);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private static void renameSchemaTypeTypeNames(
            final GraphQLRouteTypeNameStrategy typeNameStrategy,
            final Map<String, Object> type) {

        if (type == null) {
            return;
        }

        final String kind = (String) type.get("kind");
        if (kind == null || !kind.equals("SCALAR")) {
            renameRouteToClientTypeName(typeNameStrategy, type, "name");
        }

        final List<Map<String, Object>> fields = (List<Map<String, Object>>) type.get("fields");
        if (fields != null) {
            for (final Map<String, Object> field : fields) {
                renameSchemaFieldTypes(typeNameStrategy, field);
            }
        }

        final Map<String, Object> ofType = (Map<String, Object>) type.get("ofType");
        renameSchemaTypeTypeNames(typeNameStrategy, ofType);

        final List<Map<String, Object>> interfaces = (List<Map<String, Object>>) type.get("interfaces");
        renameSchemaTypeTypeNames(typeNameStrategy, interfaces);

        final List<Map<String, Object>> possibleTypes = (List<Map<String, Object>>) type.get("possibleTypes");
        renameSchemaTypeTypeNames(typeNameStrategy, possibleTypes);
    }

    @SuppressWarnings("unchecked")
    private static void renameSchemaFieldTypes(final GraphQLRouteTypeNameStrategy typeNameStrategy, final Map<String, Object> field) {
        final Map<String, Object> fieldType = (Map<String, Object>) field.get("type");
        if (fieldType != null) {
            renameSchemaTypeTypeNames(typeNameStrategy, fieldType);
        }

        final List<Map<String, Object>> args = (List<Map<String, Object>>) field.get("args");
        if (args != null) {
            for (final Map<String, Object> arg : args) {
                final Map<String, Object> argType = (Map<String, Object>) arg.get("type");
                if (argType != null) {
                    renameSchemaTypeTypeNames(typeNameStrategy, argType);
                }
            }
        }
    }

    private static void renameRouteToClientTypeName(
            final GraphQLRouteTypeNameStrategy typeNameStrategy,
            final Map<String, Object> map,
            final String fieldName) {

        if (map == null) {
            return;
        }
        final String typeName = (String) map.get(fieldName);
        if (typeName != null && !typeName.startsWith("__")) {
            final String newTypeName = typeNameStrategy.translateTypeNameRouteToClient(typeName);
            if (!Objects.equals(typeName, newTypeName)) {
                map.put(fieldName, newTypeName);
            }
        }
    }
}
