package com.atlassian.graphql.json;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLIDType;
import com.atlassian.graphql.annotations.GraphQLIgnore;
import com.atlassian.graphql.annotations.GraphQLTypeName;
import com.atlassian.graphql.json.types.JsonOrXmlRootGraphQLTypeBuilder;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableMap;
import graphql.schema.GraphQLNamedType;
import graphql.schema.idl.SchemaPrinter;
import org.junit.Test;

import javax.annotation.Generated;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import java.util.EnumSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ImmutableValueTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.noneOf(GraphQLJsonSerializerOptions.class));
    private final GraphQLTestTypeBuilderImpl typeBuilder = new GraphQLTestTypeBuilderImpl(new JsonOrXmlRootGraphQLTypeBuilder());

    @Test
    public void testSchema() {
        final String schema = new GraphQLTypeSchemaPrinter().print(
                (GraphQLNamedType) typeBuilder.buildType(TestImmutableValue.class).getType());
        assertEquals(
                "type TestImmutableValue {\n" +
                "  field: [NamedStringToStringType]\n" +
                "  id: ID\n" +
                "}\n" +
                "\n" +
                "type NamedStringToStringType {\n" +
                "  key: String\n" +
                "  value: String\n" +
                "}",
                schema);
    }

    @Test
    public void testSerialize() throws Exception {
        final TestImmutableValue obj = new ImmutableTestImmutableValue();
        assertEquals(
                "{\n" +
                "  \"field\" : [ {\n" +
                "    \"key\" : \"key\",\n" +
                "    \"value\" : \"value\"\n" +
                "  } ],\n" +
                "  \"id\" : \"id\"\n" +
                "}",
                serializer.serializeUsingGraphQL(typeBuilder, TestImmutableValue.class, obj));
    }

    /**
     * This is what a Json object created using the java immutable library looks like.
     * See: https://immutables.github.io/immutable.html
     *
     * - @JsonSerialize instructs jackson and atlassian-graphql to serialize from the generated
     *   ImmutableTestImmutableValue subclass
     * - 'ImmutableMap' type in the generated subclass is recognized as a regular map
     * - Annotations @GraphQLIgnore, @GraphQLType, @GraphQLIDType and @GraphQLTypeName are
     *   resolved from the immutable interface type (not the generated class)
     */
    //@Value.Immutable
    @JsonPropertyOrder(alphabetic = true)
    @JsonSerialize(as = ImmutableTestImmutableValue.class)
    @JsonDeserialize(as = ImmutableTestImmutableValue.class)
    public static interface TestImmutableValue {
        @GraphQLIDType
        String id();

        @GraphQLTypeName("NamedStringToStringType")
        Map<String, String> field();

        @GraphQLIgnore
        Map<String, String> ignoredField();
    }

    /**
     * A sample immutable library generated subclass (in terms of annotations at least)
     */
    @SuppressWarnings({"all"})
    @ParametersAreNonnullByDefault
    @Generated({"Immutables.generator", "SpaceMetadata"})
    @Immutable
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ImmutableTestImmutableValue implements TestImmutableValue {
        @Override
        @JsonProperty
        public String id() {
            return "id";
        }

        @Override
        @JsonProperty
        public ImmutableMap<String, String> field() {
            return ImmutableMap.of("key", "value");
        }

        @Override
        @JsonProperty
        public ImmutableMap<String, String> ignoredField() {
            return ImmutableMap.of("key", "value");
        }
    }
}
