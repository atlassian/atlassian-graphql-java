package com.atlassian.graphql.json.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.junit.Test;

import java.util.EnumSet;

import static org.junit.Assert.assertEquals;

public class JsonSerializeAnnotatedTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerialize() throws Exception {
        final ClassWithJsonSerialize obj = new ClassWithJsonSerialize("fieldValue");
        assertEquals(
                "{\n" +
                "  \"value\" : {\n" +
                "    \"value\" : \"fieldValue\"\n" +
                "  },\n" +
                "  \"value2\" : {\n" +
                "    \"value\" : \"fieldValue\"\n" +
                "  }\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ClassWithJsonSerialize.class, obj));
    }

    public static class ClassWithJsonSerialize {
        private JsonObject value;
        private JsonObject2 value2;

        public ClassWithJsonSerialize(final String value) {
            this.value = new JsonObjectSubclass(value);
            this.value2 = new JsonObjectSubclass2(value);
        }

        @JsonProperty
        public JsonObject getValue() {
            return value;
        }

        @JsonProperty
        @JsonSerialize(as = JsonObjectSubclass2.class)
        public JsonObject2 getValue2() {
            return value2;
        }
    }

    @JsonSerialize(as = JsonObjectSubclass.class)
    public static class JsonObject { }

    public static class JsonObjectSubclass extends JsonObject {
        @GraphQLName
        private String value;

        public JsonObjectSubclass(final String value) {
            this.value = value;
        }
    }

    public static class JsonObject2 { }

    public static class JsonObjectSubclass2 extends JsonObject2 {
        @GraphQLName
        private String value;

        public JsonObjectSubclass2(final String value) {
            this.value = value;
        }
    }
}
