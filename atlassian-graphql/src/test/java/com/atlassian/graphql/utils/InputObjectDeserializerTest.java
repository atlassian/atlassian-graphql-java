package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLName;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class InputObjectDeserializerTest {
    private InputObjectDeserializer deserializer;

    @Before
    public void setUp() {
        deserializer = new InputObjectDeserializer();
    }

    @Test
    public void testDeserialize() {
        final Map<String, Object> input =
                ImmutableMap.of("object", ImmutableMap.of("field", "value"),
                                "list", Lists.newArrayList(ImmutableMap.of("field", "item1")));
        final InputMessage result = (InputMessage) deserializer.deserialize(input, InputMessage.class);
        assertEquals("value", result.object.field);
        assertEquals("item1", result.list.get(0).field);
    }

    public static class BaseInputMessage {
        @GraphQLName
        TestObject object;
        @GraphQLName
        List<TestObject> list;
    }

    public static class InputMessage extends BaseInputMessage {
    }

    public static class TestObject {
        @GraphQLName
        private String field;
    }
}
