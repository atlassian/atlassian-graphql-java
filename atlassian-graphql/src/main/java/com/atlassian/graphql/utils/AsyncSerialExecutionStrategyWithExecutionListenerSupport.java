package com.atlassian.graphql.utils;

import com.atlassian.graphql.spi.ExecutionListener;
import graphql.execution.AsyncExecutionStrategy;
import graphql.execution.AsyncSerialExecutionStrategy;
import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionStrategyParameters;
import graphql.execution.FieldValueInfo;

import java.util.concurrent.CompletableFuture;

/**
 * A {@link AsyncExecutionStrategy} that calls {@link ExecutionListener} methods when implemented
 * on the root object.
 */
public class AsyncSerialExecutionStrategyWithExecutionListenerSupport extends AsyncSerialExecutionStrategy {
    @Override
    protected CompletableFuture<FieldValueInfo> resolveFieldWithInfo(
            final ExecutionContext executionContext,
            final ExecutionStrategyParameters parameters) {

        return AsyncExecutionStrategyWithExecutionListenerSupport.resolveField(
                executionContext, parameters, () -> super.resolveFieldWithInfo(executionContext, parameters));
    }
}
