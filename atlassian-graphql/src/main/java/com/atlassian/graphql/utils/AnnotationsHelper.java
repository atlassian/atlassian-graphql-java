package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLName;
import com.google.common.base.Throwables;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.WeakHashMap;

import static java.util.Objects.requireNonNull;

/**
 * Utility methods for querying graphql or json (Jackson) annotations.
 */
public class AnnotationsHelper {
    private static final WeakHashMap<Object, Object> annotationCache = new WeakHashMap<>();

    /**
     * Get whether an annotation of the specified type exists on a reflection element.
     *
     * @param annotated       The {@link AnnotatedElement} to get the annotation from
     * @param annotationClass The type of the annotation
     * @return true if the annotation exists
     */
    public static <T extends Annotation> boolean hasAnnotation(
            final AnnotatedElement annotated,
            final Class<T> annotationClass) {

        requireNonNull(annotated);
        requireNonNull(annotationClass);

        checkSupportedAnnotation(annotationClass);
        for (Annotation annotation : getAnnotationsCached(annotated)) {
            if (annotationTypesEqual(annotation.annotationType(), annotationClass)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns an element's annotation of the specified type in an OSGi safe, and
     * The 'annotationClass' may have a different
     * class loader from 'annotations', in which case a proxy will be generated so that
     * the returned annotation works as expected.
     *
     * For methods, annotations are searched starting from the most specific type to the
     * super type and any implemented interfaces.
     *
     * @param declaringClass  The most specific subclass from which to start the search, or null
     * @param annotated       The {@link AnnotatedElement} to get the annotation from
     * @param annotationClass The type of the annotation
     * @return the element's annotation for the specified annotation type if
     * present on this element, else null
     */
    public static <T extends Annotation> T getAnnotationIncludingInherited(
            Class declaringClass,
            final AnnotatedElement annotated,
            final Class<T> annotationClass) {

        if (declaringClass == null && annotated instanceof Method) {
            declaringClass = ((Method) annotated).getDeclaringClass();
        }
        return getAnnotationIncludingInherited(declaringClass, annotated, annotationClass, true);
    }

    private static <T extends Annotation> T getAnnotationIncludingInherited(
            final Class declaringClass,
            final AnnotatedElement annotated,
            final Class<T> annotationClass,
            final boolean includeInterfaces) {

        T annotation = getAnnotation(annotated, annotationClass);
        if (annotation != null) {
            return annotation;
        }

        if (!(annotated instanceof Method) || declaringClass == null || declaringClass == Object.class) {
            return null;
        }

        // get the method from declaringClass (which may be a subclass of method.getDeclaringClass())
        final Method method = (Method) annotated;
        final Method currentMethod = ReflectionUtils.getMethodOnClass(declaringClass, method);
        if (currentMethod == null) {
            return null;
        }

        // find the annotation from the superclass
        annotation = getAnnotationIncludingInherited(declaringClass.getSuperclass(), currentMethod, annotationClass, false);
        if (annotation != null) {
            return annotation;
        }

        // find the annotation from any interfaces
        if (!includeInterfaces) {
            return null;
        }
        for (final Class interfaceType : declaringClass.getInterfaces()) {
            annotation = getAnnotationIncludingInherited(interfaceType, currentMethod, annotationClass, false);
            if (annotation != null) {
                return annotation;
            }
        }
        return null;
    }

    /**
     * Returns an element's annotation of the specified type in an OSGi safe, and
     * The 'annotationClass' may have a different
     * class loader from 'annotations', in which case a proxy will be generated so that
     * the returned annotation works as expected.
     *
     * @param annotations     The {@link Annotation}s array to get the annotation from
     * @param annotationClass The type of the annotation
     * @return the element's annotation for the specified annotation type if
     * present on this element, else null
     */
    public static <T extends Annotation> T getAnnotation(
            final Annotation[] annotations,
            final Class<T> annotationClass) {

        requireNonNull(annotations);
        requireNonNull(annotationClass);

        checkSupportedAnnotation(annotationClass);
        for (Annotation annotation : annotations) {
            // when annotation classes are compatible
            if (annotation.annotationType() == annotationClass) {
                return (T) annotation;
            }

            // when the annotation classes have incompatible class loaders, create a proxy.
            if (annotationTypesEqual(annotation.annotationType(), annotationClass)) {
                return createAnnotationProxy(annotationClass, annotation);
            }
        }
        return null;
    }

    /**
     * Returns an element's graph-ql annotation of the specified type in an OSGi safe,
     * The 'annotationClass' may have a different
     * class loader from 'annotated', in which case a proxy will be generated so that
     * the returned annotation works as expected.
     *
     * @param annotated       The {@link AnnotatedElement} to get the annotation from
     * @param annotationClass The type of the annotation
     * @return the element's annotation for the specified annotation type if
     * present on this element, else null
     */
    public static <T extends Annotation> T getAnnotation(
            final AnnotatedElement annotated,
            final Class<T> annotationClass) {

        requireNonNull(annotated);
        requireNonNull(annotationClass);
        return getAnnotation(getAnnotationsCached(annotated), annotationClass);
    }

    public static boolean isJsonAnnotation(final Class annotationClass) {
        final String packageName = annotationClass.getPackage().getName();
        return (packageName.startsWith("com.fasterxml.jackson.")
                || packageName.startsWith("com.atlassian.graphql.mock.jackson"))
                && (packageName.endsWith(".annotation"));
    }

    private static <T extends Annotation> void checkSupportedAnnotation(final Class<T> annotationClass) {
        if (!annotationClass.getPackage().getName().startsWith(GraphQLName.class.getPackage().getName())
                && !annotationClass.getName().equals(XmlElement.class.getName())
                && !annotationClass.getName().equals(XmlAttribute.class.getName())
                && !isJsonAnnotation(annotationClass)) {

            throw new IllegalArgumentException(
                    AnnotationsHelper.class.getSimpleName()
                            + " is only supported for graph-ql or Jackson annotations");
        }
    }

    /**
     * When the annotation classes have incompatible class loaders create a proxy.
     */
    private static <T extends Annotation> T createAnnotationProxy(
            final Class<T> annotationClass,
            final Annotation annotation) {

        final Object cachedValueField = getAnnotationValueField(annotation, annotationClass);
        final Object annotationProxy = Proxy.newProxyInstance(
                annotationClass.getClassLoader(),
                new Class[]{annotationClass},
                (proxy, method, args) -> {
                    if (method.getName().equals("value")) {
                        return cachedValueField;
                    }
                    final Method annotationMethod = ReflectionUtils.getMethodOnClass(
                            annotation.getClass(), method);
                    if (annotationMethod == null) {
                        //nulls can happen when mixing different versions of the same library with new method definitions
                        return null;
                    } else {
                        final Object result = annotationMethod.invoke(annotation, args);
                        return adaptAnnotationMethodResult(result, method.getReturnType());
                    }
                });
        return (T) annotationProxy;
    }

    /**
     * Invoke 'annotation.value()' so that it can be cached, it's kinda slow to evaluate for JsonSubTypes.
     */
    private static Object getAnnotationValueField(final Annotation annotation, final Class<?> annotationClass) {
        try {
            final Method method = annotation.getClass().getMethod("value");
            final Method proxyMethod = annotationClass.getMethod("value");
            return adaptAnnotationMethodResult(method.invoke(annotation), proxyMethod.getReturnType());
        } catch (final NoSuchMethodException ex) {
            return null;
        } catch (ReflectiveOperationException ex) {
            throw Throwables.propagate(ex);
        }
    }

    private static Object adaptAnnotationMethodResult(final Object obj, final Class returnType) {
        // required for @JsonTypeInfo
        if (obj instanceof Enum) {
            return Enum.valueOf((Class<Enum>) returnType, ((Enum) obj).name());
        }

        // required for @JsonSubTypes
        if (obj instanceof Annotation) {
            return createAnnotationProxy((Class<Annotation>) returnType, (Annotation) obj);
        }

        // required for @JsonSubTypes
        if (obj instanceof Object[]) {
            final Object[] array = (Object[]) obj;
            final Object[] arrayResult = (Object[]) Array.newInstance(returnType.getComponentType(), array.length);
            for (int i = 0; i < array.length; i++) {
                arrayResult[i] = adaptAnnotationMethodResult(array[i], returnType.getComponentType());
            }
            return arrayResult;
        }
        return obj;
    }

    private static Annotation[] getAnnotationsCached(final AnnotatedElement annotated) {
        Annotation[] result = (Annotation[]) annotationCache.get(annotated);
        if (result == null) {
            result = annotated.getAnnotations();
            annotationCache.put(annotated, result);
        }
        return result;
    }

    private static boolean annotationTypesEqual(final Class x, final Class y) {
        return x == y
                || x.getName().equals(y.getName())
                || (isJsonAnnotation(x) && isJsonAnnotation(y) && x.getSimpleName().equals(y.getSimpleName()));
    }
}
