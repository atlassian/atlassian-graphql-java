package com.atlassian.graphql.provider.internal;

import com.atlassian.graphql.GraphQLFetcherException;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.execution.ResultPath;
import graphql.language.SourceLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionException;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Format a list of @{link GraphQLError} to a map, by categorizing the errors by path.
 */
public class GraphQLErrorFormatter {
    public static List<Object> formatErrorsMap(
            final List<GraphQLError> errors,
            final Function<Exception, Object> errorConverter) {

        requireNonNull(errors);

        final List<Object> list = new ArrayList<>();
        for (final GraphQLError error : errors) {
            addError(list, error, ex -> {
                Object result = errorConverter != null ? errorConverter.apply(ex) : null;
                if (result == null) {
                    result = ex.getClass().getName() + "; " + ex.getMessage();
                }
                return result;
            });
        }
        return list;
    }

    private static void addError(
            final List<Object> list,
            final GraphQLError error,
            final Function<Exception, Object> errorConverter) {

        if (error instanceof ExceptionWhileDataFetching) {
            final ExceptionWhileDataFetching exceptionError = (ExceptionWhileDataFetching) error;
            if (exceptionError.getException() instanceof GraphQLFetcherException) {
                final GraphQLFetcherException ex = (GraphQLFetcherException) exceptionError.getException();
                list.add(addFieldNameToDetails(ex.getPath(), errorConverter.apply((Exception) ex.getCause())));
            } else if (exceptionError.getException() instanceof CompletionException) {
                // @GraphQLBatchLoader throws CompletionException instead of GraphQLFetcherException
                final CompletionException ex = (CompletionException) exceptionError.getException();
                list.add(addFieldNameToDetails(ResultPath.fromList(exceptionError.getPath()), errorConverter.apply((Exception) ex.getCause())));
            } else if (exceptionError.getException() instanceof RuntimeException) {
                // to capture custom exception from client
                final RuntimeException ex = (RuntimeException) exceptionError.getException();
                list.add(addFieldNameToDetails(ResultPath.fromList(exceptionError.getPath()), errorConverter.apply(ex)));
            } else {
                list.add(ImmutableMap.of("message", Throwables.getStackTraceAsString(exceptionError.getException())));
            }
        } else {
            list.add(toMap(error));
        }
    }

    private static Object addFieldNameToDetails(final ResultPath path, final Object details) {
        final Map<String, Object> map = new HashMap<>();
        if (path != null) {
            map.put("path", path.toList());
        }
        if (details instanceof String) {
            map.put("message", details);
        } else if (details instanceof Map) {
            map.putAll((Map<String, Object>) details);
        } else {
            return details;
        }
        return map;
    }

    private static Map<String, Object> toMap(final GraphQLError error) {
        final Map<String, Object> map = new HashMap<>();
        map.put("message", error.getMessage());
        if (error.getLocations() != null) {
            map.put("locations", toList(error.getLocations()));
        }
        return map;
    }

    private static List<Map<String, Object>> toList(final List<SourceLocation> locations) {
        return locations.stream().map(GraphQLErrorFormatter::toMap).collect(Collectors.toList());
    }

    private static Map<String, Object> toMap(final SourceLocation location) {
        final Map<String, Object> map = new HashMap<>();
        map.put("column", location.getColumn());
        map.put("line", location.getLine());
        return map;
    }
}
