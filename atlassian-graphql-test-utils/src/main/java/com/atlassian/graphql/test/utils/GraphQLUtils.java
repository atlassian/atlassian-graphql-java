package com.atlassian.graphql.test.utils;

import graphql.schema.GraphQLModifiedType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.util.Map;

public class GraphQLUtils {
    /**
     * Unwrap a {@link GraphQLModifiedType} or {@link GraphQLTypeReference} type.
     */
    public static GraphQLType unwrap(final GraphQLType type, final Map<String, GraphQLType> allTypes) {
        if (type instanceof GraphQLModifiedType) {
            return unwrap(((GraphQLModifiedType) type).getWrappedType(), allTypes);
        } else if (type instanceof GraphQLTypeReference) {
            return unwrap(allTypes.get(((GraphQLTypeReference) type).getName()), allTypes);
        }
        return type;
    }
}
