package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLType;
import com.atlassian.graphql.annotations.GraphQLTypeName;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.SchemaPrinter;
import org.junit.Test;

import java.util.EnumSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EnumTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerializeUnnamedField() throws Exception {
        final ObjectWithEnumField obj = new ObjectWithEnumField(TestEnum.UNNAMED_FIELD);
        assertEquals(
                "{\n" +
                "  \"enumField\" : \"UNNAMED_FIELD\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ObjectWithEnumField.class, obj));
    }

    @Test
    public void testSerializeNamedField() throws Exception {
        final ObjectWithEnumField obj = new ObjectWithEnumField(TestEnum.NAMED_FIELD);
        assertEquals(
                "{\n" +
                "  \"enumField\" : \"NamedField\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ObjectWithEnumField.class, obj));
    }

    @Test
    public void testSchema() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithEnumField.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertTrue(
                new SchemaPrinter(SchemaPrinter.Options.defaultOptions().includeDirectives(false)).print(schema)
                .contains("type ObjectWithEnumField {\n" +
                        "  enumField: TestEnum\n" +
                        "}\n" +
                        "\n" +
                        "enum TestEnum {\n" +
                        "  NamedField\n" +
                        "  UNNAMED_FIELD\n" +
                        "}"));
    }

    @Test
    public void testSchemaWithMultipleEnumParameters() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithEnumParameter.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertTrue(
                new SchemaPrinter(SchemaPrinter.Options.defaultOptions().includeDirectives(false)).print(schema)
                .contains("type ObjectWithEnumParameter {\n" +
                        "  bar(format: TestEnum): String\n" +
                        "  foo(format: TestEnum): String\n" +
                        "}\n" +
                        "\n" +
                        "enum TestEnum {\n" +
                        "  NamedField\n" +
                        "  UNNAMED_FIELD\n" +
                        "}"));
    }

    @GraphQLTypeName("TestEnum")
    public enum TestEnum {
        UNNAMED_FIELD,
        @GraphQLName("NamedField")
        NAMED_FIELD,
    }

    public static class ObjectWithEnumField {
        @GraphQLName
        private TestEnum enumField;

        public ObjectWithEnumField(final TestEnum enumField) {
            this.enumField = enumField;
        }
    }

    @GraphQLName("ObjectWithEnumParameter")
    public static class ObjectWithEnumParameter {

        @GraphQLName("foo")
        public String foo(@GraphQLName("format") TestEnum format) {
            return "foo";
        }

        @GraphQLName("bar")
        public String bar(@GraphQLName("format") TestEnum format) {
            return "bar";
        }
    }
}
