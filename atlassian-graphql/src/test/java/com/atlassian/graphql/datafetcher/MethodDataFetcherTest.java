package com.atlassian.graphql.datafetcher;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.GraphQLFetcherException;
import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLDefaultValue;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.expansions.GraphQLExpandable;
import com.atlassian.graphql.annotations.expansions.GraphQLExpansionParam;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.atlassian.graphql.utils.AsyncExecutionStrategyWithExecutionListenerSupport;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.Scalars;
import graphql.execution.ResultPath;
import graphql.execution.ExecutionStepInfo;
import graphql.schema.DataFetchingEnvironment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Method;
import java.util.EnumSet;
import java.util.Formatter;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MethodDataFetcherTest {
    @Mock
    private DataFetchingEnvironment dataFetchingEnvironment;

    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.noneOf(GraphQLJsonSerializerOptions.class));

    @Test
    public void testGet() throws Exception {
        when(dataFetchingEnvironment.getSource()).thenReturn(new TestObject("value"));
        final Method method = TestObject.class.getMethod("getField");
        final MethodDataFetcher dataFetcher = new MethodDataFetcher(method);
        assertEquals("value", dataFetcher.get(dataFetchingEnvironment));
    }

    @Test
    public void testGetThrowsGraphQLFetcherException() throws Exception {
        try {
            when(dataFetchingEnvironment.getSource()).thenReturn(new TestObject("error"));
            when(dataFetchingEnvironment.getExecutionStepInfo()).thenReturn(
                    ExecutionStepInfo.newExecutionStepInfo()
                    .type(Scalars.GraphQLString)
                    .path(ResultPath.rootPath().segment("path"))
                    .build());
            final Method method = TestObject.class.getMethod("getField");
            final MethodDataFetcher dataFetcher = new MethodDataFetcher(method);
            dataFetcher.get(dataFetchingEnvironment);
            fail("Expected a GraphQLFetcherException");
        } catch (final GraphQLFetcherException ex) {
            assertEquals("path", ResultPath.rootPath().segment("path"), ex.getPath());
        }
    }

    @Test
    public void testUseSourceFromConstructor() throws Exception {
        final Method method = TestObject.class.getMethod("getField");
        final MethodDataFetcher dataFetcher = new MethodDataFetcher(method, "field", new TestObject("value"), null, null, null, null);
        assertEquals("value", dataFetcher.get(dataFetchingEnvironment));
    }

    @Test
    public void testInjectParameterValues() throws Exception {
        when(dataFetchingEnvironment.getContext()).thenReturn(new GraphQLContext(Formatter.class, new Formatter()));
        final Method method = TestObject.class.getMethod("getFieldWithInjectedParameters", DataFetchingEnvironment.class, Formatter.class);
        final MethodDataFetcher dataFetcher = new MethodDataFetcher(method, "fieldWithInjectedParameters", new TestObject("value"), null, null, null, null);
        assertEquals("done", dataFetcher.get(dataFetchingEnvironment));
    }

    @Test
    public void testGetDefaultParameterValue() throws Exception {
        final TestObject obj = new TestObject();
        final Object response = serializer.serializeToObjectUsingGraphQL(
                new GraphQLTestTypeBuilderImpl(),
                TestObject.class,
                obj,
                "{ fieldWithDefault }");
        assertEquals("default", ((Map) response).get("fieldWithDefault"));
    }

    @Test
    public void testExpansions() throws Exception {
        final TestObject obj = new TestObject();
        final GraphQLContext graphqlContext = new GraphQLContext();
        final Object response = serializer.serializeToObjectUsingGraphQL(
                new GraphQLTestTypeBuilderImpl(),
                TestObject.class,
                graphqlContext,
                obj,
                "{ fieldWithExpansions { field1, field2, expand } }",
                new AsyncExecutionStrategyWithExecutionListenerSupport());
        final Map<String, Object> expandable = (Map<String, Object>) ((Map) response).get("fieldWithExpansions");
        assertEquals("field1", expandable.get("field1"));
        assertEquals("field2", expandable.get("field2"));
        assertEquals("field1,field2", expandable.get("expand"));
    }

    public static class TestObject {
        private String field;

        public TestObject() {
        }

        public TestObject(final String field) {
            this.field = field;
        }

        @GraphQLName
        public String getField() {
            if ("error".equals(field)) {
                throw new RuntimeException("badness");
            }
            return field;
        }

        @GraphQLName
        public String getFieldWithDefault(
                @GraphQLName("argument") @GraphQLDefaultValue("default")
                final String argument) {

            return argument;
        }

        @GraphQLName
        public String getFieldWithInjectedParameters(
                final DataFetchingEnvironment env,
                final Formatter formatter) {

            assertNotNull("DataFetchingEnvironment must be injected", env);
            assertNotNull("Formatter must be injected", formatter);
            return "done";
        }

        @GraphQLName
        public ExpandableObject getFieldWithExpansions(@GraphQLExpansionParam final String expand) {
            return new ExpandableObject("field1", "field2", expand);
        }
    }

    public static class ExpandableObject {
        @GraphQLName
        @GraphQLExpandable
        public final String field1;

        @GraphQLName
        @GraphQLExpandable
        public final String field2;

        @GraphQLName
        public final String expand;

        public ExpandableObject(final String field1, final String field2, final String expand) {
            this.field1 = field1;
            this.field2 = field2;
            this.expand = expand;
        }
    }
}
