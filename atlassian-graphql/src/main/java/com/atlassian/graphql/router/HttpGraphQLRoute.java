package com.atlassian.graphql.router;

import com.atlassian.graphql.rest.GraphQLRestRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * A GraphQL route that calls a HTTP GraphQL server.
 *
 * Note: The route method is not asynchronous in it's current implementation
 */
public class HttpGraphQLRoute implements GraphQLRoute {
    private final ObjectMapper mapper = new ObjectMapper();
    private final GraphQLRouteRules rules;
    private final URL endpoint;

    public HttpGraphQLRoute(final GraphQLRouteRules rules, final URL endpoint) {
        this.rules = rules;
        this.endpoint = endpoint;
    }

    @Override
    public GraphQLRouteRules getRouteRules() {
        return rules;
    }

    @Override
    public CompletableFuture<Object> route(final GraphQLRestRequest request, Object context) throws IOException {
        final URLConnection urlConnection = endpoint.openConnection();
        if (!(urlConnection instanceof HttpURLConnection)) {
            throw new IOException("Expected an HTTP endpoint");
        }
        final HttpURLConnection connection = (HttpURLConnection) urlConnection;
        try {
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("content-type", "application/json");

            mapper.writer().writeValue(connection.getOutputStream(), request);
            if (connection.getResponseCode() != 200) {
                throw new IOException("Failed with HTTP error code: " + connection.getResponseCode());
            }
            final Map response = mapper.reader(Map.class).readValue(connection.getInputStream());
            return CompletableFuture.completedFuture(response);
        } finally {
            connection.disconnect();
        }
    }
}
