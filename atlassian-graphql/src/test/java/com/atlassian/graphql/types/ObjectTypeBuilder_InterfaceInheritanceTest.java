package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLInterface;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.collect.ImmutableMap;
import graphql.TypeResolutionEnvironment;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.TypeResolver;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.reflect.Type;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ObjectTypeBuilder_InterfaceInheritanceTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    TypeResolutionEnvironment mockEnv;

    private final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();

    private Map<Type, GraphQLOutputType> getPossibleTypes(ObjectTypeBuilder typeBuilder) {
        return ImmutableMap.of(Base.class, (GraphQLOutputType)typeBuilder.buildType(Base.class, context),
                Derived.class, (GraphQLOutputType)typeBuilder.buildType(Derived.class, context),
                AnotherBase.class, (GraphQLOutputType)typeBuilder.buildType(AnotherBase.class, context));
    }

    @Test
    public void testDerivedInstTypeIsDeducedCorrectly() {
        when(mockEnv.getObject()).thenReturn(new Derived());
        ObjectTypeBuilderStub typeBuilder = new ObjectTypeBuilderStub();
        Map<Type, GraphQLOutputType> possibleTypes = getPossibleTypes(typeBuilder);

        TypeResolver typeResolver = typeBuilder.buildDefaultTypeResolver(Derived.class, possibleTypes, context);
        GraphQLObjectType type = typeResolver.getType(mockEnv);

        assertEquals(type.getName(), "Derived");
    }

    @Test
    public void testBaseInstTypeIsDeducedCorrectly() {
        when(mockEnv.getObject()).thenReturn(new AnotherBase());
        ObjectTypeBuilderStub typeBuilder = new ObjectTypeBuilderStub();
        Map<Type, GraphQLOutputType> possibleTypes = getPossibleTypes(typeBuilder);

        TypeResolver typeResolver = typeBuilder.buildDefaultTypeResolver(Derived.class, possibleTypes, context);
        GraphQLObjectType type = typeResolver.getType(mockEnv);

        assertEquals(type.getName(), "AnotherBase");
    }

    @GraphQLInterface(possibleTypes = { Base.class, Derived.class, AnotherBase.class })
    public static abstract class InterfaceType {
        @GraphQLName
        private final String type = "InterfaceType";
    }

    public static class Base extends InterfaceType {
        @GraphQLName
        private final String type = "BaseType";
    }

    public static class Derived extends Base {
        @GraphQLName
        private final String type = "DerivedType";
    }

    public static class AnotherBase extends InterfaceType {
        @GraphQLName
        private final String type = "AnotherBaseType";
    }

    /**
     * A stub of the {@link ObjectTypeBuilder} class to make a protected method available for testing.
     */
    public static class ObjectTypeBuilderStub extends ObjectTypeBuilder {

        public ObjectTypeBuilderStub() {
            super(new RootGraphQLTypeBuilder(), null);
        }

        @Override
        public TypeResolver buildDefaultTypeResolver(
                final Type type,
                final Map<Type, GraphQLOutputType> possibleTypes,
                final GraphQLTypeBuilderContext context) {
            return super.buildDefaultTypeResolver(type, possibleTypes, context);
        }
    }
}
