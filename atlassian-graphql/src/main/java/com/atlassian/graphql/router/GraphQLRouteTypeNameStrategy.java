package com.atlassian.graphql.router;

/**
 * A strategy for translating type names as they're passed from the client to server and back again.
 */
public interface GraphQLRouteTypeNameStrategy {
    /**
     * Translate the type name from a GraphQLRouter type name in a query to the route endpoint type name.
     * @param typeName The type name as seen from the GraphQLRouter's schema
     * @return The type name as seen from the route endpoint
     */
    String translateTypeNameClientToRoute(final String typeName);

    /**
     * Translate the type name from a route endpoint schema response to the GraphQLRouter schema.
     * @param typeName The type name as seen from the route endpoint
     * @return The type name as seen from the GraphQLRouter's schema
     */
    String translateTypeNameRouteToClient(final String typeName);
}
