package com.atlassian.graphql.utils;

import graphql.ExecutionInput;
import graphql.execution.preparsed.PreparsedDocumentEntry;
import graphql.language.Document;
import graphql.parser.Parser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GraphQLQueryCacheTest {
    @Test
    public void testCacheQuery() {
        final String staticQuery1 = "{ id title }";
        final String staticQuery2 = "{ likes { currentUserLikes } }";
        final String dynamicQuery1 = "{ content(id: 123) { status } }";
        final String dynamicQuery2 = "{ content(id: 234) { status } }";
        final String dynamicQuery3 = "{ content(id: 345) { status } }";
        final String dynamicQuery4 = "{ content(id: 456) { status } }";
        final String dynamicQuery5 = "{ content(id: 567) { status } }";
        final String dynamicQuery6 = "{ content(id: 678) { status } }";

        final GraphQLQueryCache cache = new GraphQLQueryCache(staticQuery1.length() + staticQuery2.length() + dynamicQuery1.length() + 100);
        final Document staticDocument1 = get(cache, staticQuery1);
        final Document staticDocument2 = get(cache, staticQuery2);
        final Document dynamicDocument1 = get(cache, dynamicQuery1);
        assertEquals("staticQuery1 is cached", true, staticDocument1 == get(cache, staticQuery1));
        assertEquals("dynamicQuery1 is cached", true, dynamicDocument1 == get(cache, dynamicQuery1));
        assertEquals("staticQuery2 is cached", true, staticDocument2 == get(cache, staticQuery2));

        final Document dynamicDocument2 = get(cache, dynamicQuery2);
        assertEquals("staticQuery1 is cached", true, staticDocument1 == get(cache, staticQuery1));
        assertEquals("dynamicQuery2 is cached", true, dynamicDocument2 == get(cache, dynamicQuery2));
        assertEquals("staticQuery1 is cached", true, staticDocument1 == get(cache, staticQuery1));
        assertEquals("staticQuery2 is cached", true, staticDocument2 == get(cache, staticQuery2));

        get(cache, dynamicQuery3);
        get(cache, dynamicQuery4);
        get(cache, dynamicQuery5);
        get(cache, dynamicQuery6);
        assertEquals("dynamicQuery1 evicted", false, dynamicDocument1 == get(cache, dynamicQuery1));
        assertEquals("staticQuery1 is cached", true, staticDocument1 == get(cache, staticQuery1));
    }

    @Test
    public void testParseError() {
        final GraphQLQueryCache cache = new GraphQLQueryCache(10);
        PreparsedDocumentEntry result = cache.getDocument(ExecutionInput.newExecutionInput().query("{}}").build(),
                q -> new PreparsedDocumentEntry(new Parser().parseDocument(q.getQuery())));
        assertEquals(
                "InvalidSyntaxError{ message=Invalid syntax with offending token '}' at line 1 column 2 ,offendingToken=} ,locations=[SourceLocation{line=1, column=2}] ,sourcePreview={}}\n" +
                        "}",
                result.getErrors().get(0).toString());
    }

    private static Document get(final GraphQLQueryCache cache, final String query) {
        return cache.getDocument(ExecutionInput.newExecutionInput().query(query).build(),
                q -> new PreparsedDocumentEntry(new Parser().parseDocument(q.getQuery()))).getDocument();
    }
}
