package com.atlassian.graphql.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When applied to a field or method, indicates that the field or method represents a field
 * in the graph.
 *
 * When applied to a method parameter, indicates a binding between the parameter and
 * the name of the graph-ql argument.
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphQLName {
    String value() default "";
    int order() default -1;
}
