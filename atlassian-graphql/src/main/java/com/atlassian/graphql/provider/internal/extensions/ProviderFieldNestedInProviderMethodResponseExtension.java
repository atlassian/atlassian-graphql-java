package com.atlassian.graphql.provider.internal.extensions;

import com.atlassian.graphql.provider.RootProviderGraphQLTypeBuilder;
import com.atlassian.graphql.provider.internal.ProviderFieldTree;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLFieldDefinition;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * An extension that supports the edge case of embedding a provider field within the response
 * field tree of another provider field.
 */
public class ProviderFieldNestedInProviderMethodResponseExtension implements GraphQLExtensions {
    private final RootProviderGraphQLTypeBuilder providerClassGraphQLTypeBuilder;
    private final ProviderFieldTree fieldTree;
    private final BiConsumer<Object, Exception> errorHandler;
    private GraphQLExtensions allExtensions;

    public ProviderFieldNestedInProviderMethodResponseExtension(
            final RootProviderGraphQLTypeBuilder providerClassGraphQLTypeBuilder,
            final ProviderFieldTree fieldTree,
            final BiConsumer<Object, Exception> errorHandler) {

        this.providerClassGraphQLTypeBuilder = requireNonNull(providerClassGraphQLTypeBuilder);
        this.fieldTree = requireNonNull(fieldTree);
        this.errorHandler = errorHandler;
    }

    public void setAllExtensions(final GraphQLExtensions allExtensions) {
        this.allExtensions = allExtensions;
    }

    @Override
    public String contributeTypeName(
            final String typeName,
            final Type type,
            final GraphQLTypeBuilderContext context) {

        final ProviderFieldTree providerField =
                fieldTree.getFieldFromPath(context.getCurrentFieldPath());
        if (providerField == null) {
            return null;
        }

        // update the type name, because with new fields it's a completely different type
        final List<GraphQLFieldDefinition> newFields = buildFields(providerField, context);
        if (!newFields.isEmpty()) {
            List<String> fieldNames =
                    newFields.stream()
                    .map(field -> capitalizeFirstLetter(field.getName()))
                    .collect(Collectors.toList());
            return typeName + "With" + String.join("", fieldNames);
        }
        return null;
    }

    private static String capitalizeFirstLetter(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    @Override
    public void contributeFields(
            String typeName,
            final Type type,
            final List<GraphQLFieldDefinition> fields,
            final GraphQLTypeBuilderContext context) {

        final ProviderFieldTree providerField =
                fieldTree.getFieldFromPath(context.getCurrentFieldPath());
        if (providerField != null) {
            fields.addAll(buildFields(providerField, context));
        }
    }

    private List<GraphQLFieldDefinition> buildFields(
            final ProviderFieldTree providerField,
            final GraphQLTypeBuilderContext context) {

        final List<GraphQLFieldDefinition> newFields = new ArrayList<>();
        for (final ProviderFieldTree childField : providerField.getFields()) {
            providerClassGraphQLTypeBuilder.buildFields(
                    newFields,
                    childField,
                    context.getTypeBuilder(),
                    context,
                    allExtensions,
                    errorHandler);
        }
        return newFields;
    }
}
