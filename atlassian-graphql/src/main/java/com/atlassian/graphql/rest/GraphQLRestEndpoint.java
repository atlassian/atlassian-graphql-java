package com.atlassian.graphql.rest;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.GraphQLExecutor;
import com.atlassian.graphql.provider.internal.GraphQLErrorFormatter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.ExecutionResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import static java.util.Objects.requireNonNull;

/**
 * Provides a REST API endpoint for GraphQL.
 */
public class GraphQLRestEndpoint {
    private final ObjectMapper requestObjectMapper = new ObjectMapper();
    private final GraphQLExecutor executor;
    private final BiFunction<GraphQLRestRequest, Exception, Object> errorConverter;

    /**
     * Constructor
     * @param executor The object responsible for running the graph-ql query
     * @param errorConverter Used to convert an error before applying it to the response
     */
    public GraphQLRestEndpoint(final GraphQLExecutor executor, final BiFunction<GraphQLRestRequest, Exception, Object> errorConverter) {
        this.executor = executor;
        this.errorConverter = errorConverter;
    }

    /**
     * Execute a graph-ql REST request.
     * @param requestString The graph-ql request in mime application/graphql format
     * @param context The context variables to pass to provider methods
     * @return The REST response
     * @throws IOException If there was an error executing the request
     */
    public Object execute(
            final String requestString,
            final GraphQLContext context) throws IOException {

        requireNonNull(requestString);
        requireNonNull(context);

        try {
            // try parsing in 'multiple request' format
            final List<GraphQLRestRequest> queries = GraphQLRestRequest.parseMultiple(requestString, requestObjectMapper);
            return executeList(queries, context);
        } catch (JsonMappingException ex) {
            // otherwise try parsing in 'single request' format
            final GraphQLRestRequest query = GraphQLRestRequest.parseSingle(requestString, requestObjectMapper);
            return execute(query, context);
        }
    }

    /**
     * Execute a graph-ql REST request as an array of requests.
     * @param requests The graph-ql requests
     * @param context The context variables to pass to provider methods
     * @return The REST response
     * @throws IOException If there was an error executing the request
     */
    public List<Map<String, Object>> executeList(final List<GraphQLRestRequest> requests, final GraphQLContext context)
            throws IOException {

        final List<Map<String, Object>> list = new ArrayList<>();
        for (final GraphQLRestRequest query : requests) {
            list.add(execute(query, context));
        }
        return list;
    }

    /**
     * Execute a graph-ql REST request.
     * @param request The graph-ql request
     * @param context The context variables to pass to provider methods
     * @return The REST response
     * @throws IOException If there was an error executing the request
     */
    public Map<String, Object> execute(final GraphQLRestRequest request, final GraphQLContext context)
            throws IOException {

        final ExecutionResult executionResult = executor.execute(request, context);

        // add data to the response
        final Map<String, Object> response = new LinkedHashMap<>();
        if (executionResult.getData() != null) {
            final Map<String, Object> data = executionResult.getData();
            response.put("data", data);
        }

        if (executionResult.getExtensions() != null) {
            response.put("extensions", executionResult.getExtensions());
        }

        // add variables
        if (request.getVariables().size() > 0) {
            response.put("variables", request.getVariables());
        }

        // add errors
        if (!executionResult.getErrors().isEmpty()) {
            response.put("errors", GraphQLErrorFormatter.formatErrorsMap(executionResult.getErrors(), (ex) -> errorConverter.apply(request, ex)));
        }
        return response;
    }
}
