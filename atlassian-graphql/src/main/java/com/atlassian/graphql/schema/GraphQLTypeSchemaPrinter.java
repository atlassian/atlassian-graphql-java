package com.atlassian.graphql.schema;

import com.atlassian.graphql.utils.GraphQLSchemaMetadata;
import com.google.common.collect.Sets;
import graphql.Scalars;
import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLEnumType;
import graphql.schema.GraphQLEnumValueDefinition;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLFieldsContainer;
import graphql.schema.GraphQLInterfaceType;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLModifiedType;
import graphql.schema.GraphQLNamedOutputType;
import graphql.schema.GraphQLNamedType;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;
import graphql.schema.GraphQLUnionType;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * A simple printer for graphql schema objects. It's only intended as a testing tool for programmatically built schema.
 * 
 * @deprecated Use graphql.language.idl.SchemaPrinter as it handles full schema printing as well as type printing
 */
public class GraphQLTypeSchemaPrinter {
    private static final Comparator<GraphQLType> TYPE_COMPARATOR = nullsLast(comparing(GraphQLTypeSchemaPrinter::getName));

    private final Style style;

    public enum Style {
        /**
         * Print the type in a hierarchical fashion, but without repeatedly
         * printing the same type fields.
         */
        Hierarchical,

        /**
         * Print only the top-level fields of the type, and any nested types.
         */
        Flat
    }

    public GraphQLTypeSchemaPrinter() {
        this(Style.Flat);
    }

    public GraphQLTypeSchemaPrinter(final Style style) {
        this.style = style;
    }

    /**
     * Print the graphql schema for a {@link GraphQLType} object.
     */
    public String print(final GraphQLNamedType type) {
        return print(type, Collections.emptyMap());
    }

    /**
     * Print the graphql schema for a {@link GraphQLType} object.
     */
    public String print(final GraphQLNamedType type, final Collection<GraphQLNamedType> allTypes) {
        final Map<String, GraphQLType> allTypesMap = allTypes.stream().collect(toMap(GraphQLNamedType::getName, t -> t));
        return print(type, allTypesMap);
    }

    /**
     * Print the graphql schema for a {@link GraphQLType} object.
     */
    public String print(final GraphQLType type, final Map<String, GraphQLType> allTypes) {
        requireNonNull(type);

        final StringBuilder str = new StringBuilder();
        final Queue<GraphQLNamedType> printQueue = new ArrayDeque<>();
        printQueue.add(unwrap(type, allTypes));

        final Set<String> typesPrinted = Sets.newHashSet();
        while (!printQueue.isEmpty()) {
            final GraphQLNamedType next = printQueue.poll();
            if (next.getName() != null && typesPrinted.contains(next.getName())) {
                continue;
            }
            typesPrinted.add(next.getName());
            print(str, next, allTypes, printQueue, typesPrinted);
            if (!printQueue.isEmpty()) {
                str.append("\n\n");
            }
        }
        return str.toString();
    }

    private static void markIfExperimental(GraphQLNamedType type, StringBuilder str) {
        if (GraphQLSchemaMetadata.isExperimentalType(type)) {
            str.append("# @Experimental ").append(printFieldContainerName(type) + ": ").append(type.getName()).append("\n");
        }
    }

    private void print(
            final StringBuilder str,
            final GraphQLType type,
            final Map<String, GraphQLType> allTypes,
            final Queue<GraphQLNamedType> printQueue,
            final Set<String> typesPrinted) {

        if (type instanceof GraphQLEnumType) {
            printEnumType(str, (GraphQLEnumType) type);
        } else if (type instanceof GraphQLUnionType) {
            printUnionType(str, (GraphQLUnionType) type);
        } else if (type instanceof GraphQLFieldsContainer) {
            GraphQLFieldsContainer fieldsContainer = (GraphQLFieldsContainer) type;
            markIfExperimental(fieldsContainer, str);
            str.append(printFieldContainerName(type) + " " + fieldsContainer.getName() + printImplementsClause(type) + " {\n");
            printFieldsContainer(str, "  ", fieldsContainer, allTypes, printQueue, typesPrinted);
            str.append("}");

            if (type instanceof GraphQLInterfaceType) {
                queueInterfaceImplementations((GraphQLInterfaceType) type, allTypes.values(), printQueue);
            }
        }
    }

    private static void queueInterfaceImplementations(GraphQLInterfaceType interfaceType,
                                                      Collection<GraphQLType> types,
                                                      Queue<GraphQLNamedType> printQueue) {
        findInterfaceImplementations(interfaceType, types)
                .sorted(TYPE_COMPARATOR) // make sure to always add implementations in the same order
                .forEachOrdered(printQueue::add);
    }

    private static Stream<GraphQLObjectType> findInterfaceImplementations(GraphQLInterfaceType interfaceType,
                                                                          Collection<GraphQLType> types) {
        return findObjectTypes(types).filter(objectTypeImplementsInterface(interfaceType));
    }

    private static Stream<GraphQLObjectType> findObjectTypes(Collection<GraphQLType> types) {
        return types.stream()
                .filter(type -> type instanceof GraphQLObjectType)
                .map(GraphQLObjectType.class::cast);
    }

    private static Predicate<GraphQLObjectType> objectTypeImplementsInterface(GraphQLInterfaceType interfaceType) {
        return ot -> ot.getInterfaces().stream().anyMatch(ifce -> hasSameName(ifce, interfaceType));
    }

    private static boolean hasSameName(GraphQLNamedOutputType type1, GraphQLInterfaceType type2) {
        return Objects.equals(type1.getName(), type2.getName());
    }

    private static void printEnumType(final StringBuilder str, final GraphQLEnumType type) {
        markIfExperimental(type, str);
        str.append("enum " + type.getName() + " {\n");
        for (final GraphQLEnumValueDefinition enumValue : type.getValues()) {
            str.append("  " + enumValue.getName() + "\n");
        }
        str.append("}");
    }

    private static void printUnionType(final StringBuilder str, final GraphQLUnionType type) {
        final List<String> typeNames =
                type.getTypes().stream().map(GraphQLNamedOutputType::getName).collect(toList());
        markIfExperimental(type, str);
        str.append("union " + type.getName() + " = " + String.join(" | ", typeNames));
    }

    private static String printFieldContainerName(final GraphQLType type) {
        if (type instanceof GraphQLObjectType) {
            return "type";
        }
        if (type instanceof GraphQLInterfaceType) {
            return "interface";
        }
        throw new IllegalArgumentException("Unknown GraphQLType '" + type.getClass().getSimpleName() + "'");
    }

    private static String printImplementsClause(final GraphQLType type) {
        if (!(type instanceof GraphQLObjectType)) {
            return "";
        }
        final GraphQLObjectType objectType = (GraphQLObjectType) type;
        if (objectType.getInterfaces().isEmpty()) {

            return "";
        }

        final List<String> interfaceTypeNames =
                objectType.getInterfaces().stream().map(GraphQLNamedOutputType::getName).collect(toList());
        return " implements " + String.join(", ", interfaceTypeNames);
    }

    private void printFieldsContainer(
            final StringBuilder str,
            final String indent,
            final GraphQLFieldsContainer type,
            final Map<String, GraphQLType> allTypes,
            final Queue<GraphQLNamedType> printQueue,
            final Set<String> typesPrinted) {

        for (final GraphQLFieldDefinition field : type.getFieldDefinitions()) {
            printField(str, indent, field, allTypes, printQueue, typesPrinted);
        }
    }

    private static void markIfExperimental(GraphQLFieldDefinition field, String indent, StringBuilder str) {
        if (GraphQLSchemaMetadata.isExperimentalField(field)) {
            str.append(indent).append("# @Experimental field: ").append(field.getName()).append("\n");
        }

    }

    private void printField(
            final StringBuilder str,
            final String indent,
            final GraphQLFieldDefinition field,
            final Map<String, GraphQLType> allTypes,
            final Queue<GraphQLNamedType> printQueue,
            final Set<String> typesPrinted) {

        final GraphQLNamedType unwrappedFieldType = unwrap(field.getType(), allTypes);

        markIfExperimental(field, indent, str);
        str.append(indent).append(field.getName());
        printArguments(str, field, allTypes, printQueue, typesPrinted);
        str.append(": ").append(getName(field.getType()));

        if (!typesPrinted.contains(unwrappedFieldType.getName())) {
            if (unwrappedFieldType instanceof GraphQLFieldsContainer) {
                if (style == Style.Flat) {
                    printQueue.add(unwrappedFieldType);
                } else {
                    str.append(" {").append("\n");
                    printFieldsContainer(str, indent + "  ", (GraphQLFieldsContainer) unwrappedFieldType, allTypes, printQueue, typesPrinted);
                    str.append(indent).append("}");
                }
            } else if (unwrappedFieldType instanceof GraphQLEnumType) {
                printQueue.add(unwrappedFieldType);
            }
        }
        str.append("\n");
    }

    private static String getName(final GraphQLType type) {
        if (type instanceof GraphQLNonNull) {
            return getName(((GraphQLNonNull) type).getWrappedType()) + "!";
        }
        if (type instanceof GraphQLList) {
            return "[" + getName(((GraphQLList) type).getWrappedType()) + "]";
        }
        if (type instanceof GraphQLModifiedType) {
            return getName(((GraphQLModifiedType) type).getWrappedType());
        }
        if (type == ExtendedScalars.GraphQLChar) {
            return getName(Scalars.GraphQLString);
        }
        if (type == ExtendedScalars.GraphQLByte || type == ExtendedScalars.GraphQLShort || type == ExtendedScalars.GraphQLLong || type == ExtendedScalars.GraphQLBigInteger) {
            return getName(Scalars.GraphQLInt);
        }
        if (type == ExtendedScalars.GraphQLBigDecimal) {
            return getName(Scalars.GraphQLFloat);
        }
        return ((GraphQLNamedType) type).getName();
    }

    private void printArguments(
            final StringBuilder str,
            final GraphQLFieldDefinition field,
            final Map<String, GraphQLType> allTypes,
            final Queue<GraphQLNamedType> printQueue,
            final Set<String> typesPrinted) {

        if (field.getArguments().isEmpty()) {
            return;
        }

        field.getArguments().stream()
                .map(GraphQLArgument::getType)
                .filter(type -> !typesPrinted.contains(getName(type)))
                .map(type -> unwrap(type, allTypes))
                .forEach(printQueue::add);

        str.append("(");
        List<String> arguments = field.getArguments()
                .stream()
                .map(GraphQLTypeSchemaPrinter::toString)
                .collect(toList());
        str.append(String.join(", ", arguments));
        str.append(")");
    }

    private static String toString(final GraphQLArgument arg) {
        StringBuilder sb = new StringBuilder();
        sb.append(arg.getName())
            .append(": ")
            .append(getName(arg.getType()));
        Object defaultValue = arg.getArgumentDefaultValue().getValue();
        if (defaultValue != null) {
            sb.append(" = ");
            if (defaultValue instanceof Number) {
                sb.append(defaultValue);
            } else {
                sb.append('"').append(defaultValue).append('"');
            }
        }
        return sb.toString();
    }

    private static GraphQLNamedType unwrap(final GraphQLType type, final Map<String, GraphQLType> allTypes) {
        if (type instanceof GraphQLTypeReference) {
            final GraphQLType referencedType = allTypes.get(((GraphQLTypeReference) type).getName());
            if (referencedType == null) {
                throw new IllegalArgumentException("Failed to find referenced type with name '" + ((GraphQLTypeReference) type).getName() + "'");
            }
            return unwrap(referencedType, allTypes);
        }
        if (type instanceof GraphQLModifiedType) {
            return unwrap(((GraphQLModifiedType) type).getWrappedType(), allTypes);
        }
        
        // All GraphQLTypes are named except GraphQLModifiedTypes.
        return (GraphQLNamedType) type;
    }
}
