package com.atlassian.graphql.utils;

import graphql.language.Document;
import graphql.language.Field;
import graphql.language.OperationDefinition;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class GraphQLQueryPrinterTest {
    @Test
    public void testPrintDocument() {
        final Document document = new graphql.parser.Parser().parseDocument("{ myquery { field(arg: \"value\") } }");
        String str = new GraphQLQueryPrinter().print(document);
        assertEquals(
                "{\n" +
                "    myquery {\n" +
                "        field(arg: \"value\")\n" +
                "    }\n" +
                "}", str.trim());
    }

    @Test
    public void testPrintVariables() {
        final Document document = new graphql.parser.Parser().parseDocument("query($var: String) { myquery(arg: $var) { field } }");
        String str = new GraphQLQueryPrinter().print(document);
        assertEquals(
                "query ($var: String) {\n" +
                "    myquery(arg: $var) {\n" +
                "        field\n" +
                "    }\n" +
                "}", str.trim());
    }

    @Test
    public void testPrintField() {
        final Document document = new graphql.parser.Parser().parseDocument("{ myquery { field(arg: \"value\") } }");
        String str = new GraphQLQueryPrinter().print((Field) ((OperationDefinition) document.getChildren().get(0)).getSelectionSet().getChildren().get(0));
        assertEquals(
                "myquery {\n" +
                "    field(arg: \"value\")\n" +
                "}", str.trim());
    }

    @Test
    public void testPrintDocumentCustomizedScalars() {
        final Document document = new graphql.parser.Parser().parseDocument("{ myquery { field(arg: \"value\") } }");
        String str = new GraphQLQueryPrinter().print(document, GraphQLQueryPrinterOptions.builder()
            .scalarValuePrinter(new ScalarValuePrinter() {
                @Override
                public String print(String value) {
                    return "blah";
                }

                @Override
                public String print(BigInteger value) {
                    return value.toString();
                }

                @Override
                public String print(BigDecimal value) {
                    return value.toString();
                }

                @Override
                public String print(boolean value) {
                    return Boolean.toString(value);
                }
            })
            .build());
        assertEquals(
                "{\n" +
                        "    myquery {\n" +
                        "        field(arg: \"blah\")\n" +
                        "    }\n" +
                        "}", str.trim());
    }
}
