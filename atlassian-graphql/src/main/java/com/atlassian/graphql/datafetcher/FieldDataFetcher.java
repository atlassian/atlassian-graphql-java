package com.atlassian.graphql.datafetcher;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

import java.lang.reflect.Field;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * A {@link DataFetcher} that returns the value of a field using reflection or a Map get.
 */
public class FieldDataFetcher implements DataFetcher {
    private final String fieldName;
    private final Field field;

    public FieldDataFetcher(final String fieldName, final Field field) {
        this.fieldName = requireNonNull(fieldName);
        this.field = field;
    }

    @Override
    public Object get(final DataFetchingEnvironment env) {
        final Object source = env.getSource();
        return source != null ? getFieldValue(source) : null;
    }

    protected Object getFieldValue(final Object object) {
        if (object instanceof Map) {
            return ((Map) object).get(fieldName);
        } else if (field == null) {
            // this seems like a usage error, but we shouldn't NPE either
            return null;
        }

        if (!field.getDeclaringClass().isInstance(object)) {
            return null;
        }
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
