# Atlassian GraphQL Objects for Java

The repo is for cloud products. For DC products, use the forked repo https://bitbucket.org/atlassian/atlassian-dc-graphql-java/src/master/.

### Summary
Built on top of [graphql-java](https://github.com/graphql-java/graphql-java), atlassian-graphql provides the following capabilities:
* Build GraphQL types, interfaces and unions from ordinary Java classes
* An SPI for supporting new java types
* GraphQL REST API endpoint
* GraphQL Router to combine GraphQL services (see atlassian-graphql/src/router/README.md)
* JSON annotated class support
* Support for re-using jersey resources
* Atlassian module support

See https://extranet.atlassian.com/display/GRAPHQL/Atlassian+GraphQL+Java+Plugin for more information.

Some of this functionality overlaps with the [graphql-java-annotations](https://github.com/graphql-java/graphql-java-annotations)
java library, but this library lacks extensibility, has thread-safety issues, and has few active contributors.

### Getting Started

Add atlassian-graphql-annotations and atlassian-graphql dependencies to your pom.xml:

```maven
<dependency>
    <groupId>com.atlassian.graphql</groupId>
    <artifactId>atlassian-graphql-annotations</artifactId>
    <version>0.1.0</version>
</dependency>
<dependency>
    <groupId>com.atlassian.graphql</groupId>
    <artifactId>atlassian-graphql</artifactId>
    <version>0.1.0</version>
</dependency>
```

### Defining Objects

Any Java class may be converted to a GraphQL type. Fields are defined with the @GraphQLName annotation:

```java
public class PojoObject {
  @GraphQLName
  public String field;
}

// ...
GraphQLObjectType object = new RootGraphQLTypeBuilder().buildType(PojoObject.class);
```

Interfaces may be defined using the @GraphQLInterface annotation:

```java
@GraphQLInterface
public class PojoInterface {
  @GraphQLName
  public String field;
}

// ...
GraphQLInterfaceType object = new RootGraphQLTypeBuilder().buildType(PojoInterface.class);
```

In addition to binding a class field, a method may defined a GraphQL field:

```java
public class PojoObject {
  @GraphQLField
  public String field(@GraphQLName String argument) {
    return argument;
  }
}
```

### Adding new Java Types

Support for additional java types may be added using a GraphQLTypeBuilder and GraphQLExtensions:

```java
public class CurrencyTypeBuilder implements GraphQLTypeBuilder {
    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return Currency.class.isAssignableFrom(getClazz(type));
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return Scalars.GraphQLString;
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        return obj -> ((Currency) obj).getCurrencyCode();
    }
}

// ...
public class TypesExtension implements GraphQLExtensions {
    @Override
    public List<GraphQLTypeBuilder> getAdditionalTypeBuilders(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        return Lists.newArrayList(new CurrencyTypeBuilder());
    }
}

// ...
GraphQLScalarType object = new RootGraphQLTypeBuilder(new TypesExtension()).buildType(Currency.class);
```

### Atlassian Plugin Support

To add a GraphQL query field to an Atlassian plugin:

1. Add a &lt;graphql&gt; tag to atlassian-plugin.xml:

```xml
<graphql key="confluence-graphql-providers" name="Confluence GraphQL Providers">
    <description>Provides the GraphQL Providers for the Confluence GraphQL API</description>
    <package>com.atlassian.confluence.plugins.graphql</package>
</graphql>
```

2. Add a GraphQL provider class to the package specified in the &lt;package&gt; element:

```java
@GraphQLProvider
public class ContentGraphQLProvider {
  @GraphQLName("content")
  public Content getContent(@GraphQLName("contentId") long contentId) {
    // ...
  }
}
```

Alternatively, add a GraphQLTypeContributor to the package:

```java
@GraphQLExtensions
public class ContentGraphQLContributor extends GraphQLTypeContributor {
  public void contributeFields(final String typeName, final Type type,
                               final List<GraphQLFieldDefinition> fields, final GraphQLTypeBuilderContext context) {

    if (typeName.equals("Confluence")) {
        // build the GraphQL field and type manually
        fields.add(GraphQLFieldDefinition.newFieldDefinition(/** ... */));
    }
  }
}
```

### Mutation support

See https://graphql.org/learn/queries/#mutations for more information about GraphQL mutations.

A mutation is simply a GraphQL field that causes an update to happen on the backend.

Mutation GraphQL queries are prefixed with 'mutation' instead of 'query'.

To define a mutation field, apply @GraphQLMutation to the provider method. 

```java
@GraphQLProvider
public class ContentGraphQLProvider {
  @GraphQLMutation
  @GraphQLName("updateContent")
  public Content updateContent(@GraphQLName("request") ContentCreateRequest request) {
    // ...
  }
}
```

### Instrumentation support (eg. java-dataloader)

To provide custom Instrumentation, add a GraphQLExtensions to a plugin search package:

```
@com.atlassian.graphql.annotations.GraphQLExtensions
public class DataLoaderExtension implements GraphQLExtensions {
    private final DataLoader personDataLoader;

    public DataLoaderExtension(final PersonDataLoader personDataLoader) {
        this.personDataLoader = personDataLoader;
    }

    @Override
    public Instrumentation getInstrumentation() {
       DataLoaderRegistry registry = new DataLoaderRegistry();
       registry.register(personDataLoader);
       return new DataLoaderDispatcherInstrumentation(registry);
    }
}
```

or pass it to the constructor of RootGraphQLTypeBuilder:

```
RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder(new DataLoaderExtension());
GraphQLOutputType type = typeBuilder.buildType(ApplicationGraphQLObject.class);
```

### Modules

#### atlassian-graphql-annotations

Provides a simple annotations-based syntax for GraphQL schema and provider/controller definition.

#### atlassian-graphql

Provides the following GraphQL capabilities:
* Provides an extensible approach to building GraphQL types from Java classes through the
  GraphQLTypeBuilder interface
* GraphQL type builders for lists, maps (typed and untyped), enums, optionals and scalars
* A GraphQL REST endpoint that works with common clients such as GraphiQL and Apollo
* Extensions that allow the contribution of type names, fields, provider field fetchers,
  transformation functions and specifying how expansion fields are evaluated
* Expansion field list evaluation, to integrate with existing graph-like services that use expansions
* A GraphQL schema printer (GraphQLTypeSchemaPrinter)
* A GraphQL Router to combine GraphQL services (see atlassian-graphql/src/router/README.md)

#### atlassian-graphql ('json' package)

* Json annotations as an alternative to GraphQL-specific annotations
* Jackson fasterxml annotations supported
* Support for re-using jersey resources (for resources with expansion field support) as GraphQL providers

#### atlassian-graphql-module

* Support for Atlassian plugin <graphql> configuration element for annotated provider objects
* Support for Atlassian plugin <graphql> configuration element for extension objects
  (implementing GraphQLExtensions or GraphQLTypeContributor)

####  atlassian-graphql-test-utils

Provides tools for:
* Building a complete graph query from a GraphQL schema for testing purposes
* Comparing Json serialization to GraphQL serialization for testing purposes

### Bamboo
The bamboo build that's used to build and release atlassian-graphql can be found here:

https://confluence-cloud-bamboo.internal.atlassian.com/browse/GQL-GQL

### Contributors
Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

### License
Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.