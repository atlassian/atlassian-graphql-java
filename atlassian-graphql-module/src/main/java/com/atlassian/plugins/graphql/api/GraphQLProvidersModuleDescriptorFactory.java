package com.atlassian.plugins.graphql.api;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.google.common.base.Preconditions;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * The factory class for {@link GraphQLProvidersModuleDescriptor}, that provides the graph-ql
 * providers for plugins.
 */
@Named
@ModuleType(ListableModuleDescriptorFactory.class)
public class GraphQLProvidersModuleDescriptorFactory extends SingleModuleDescriptorFactory<GraphQLProvidersModuleDescriptor> {
    private static final String TYPE = "graphql";

    @Inject
    public GraphQLProvidersModuleDescriptorFactory(final HostContainer hostContainer) {
        super(Preconditions.checkNotNull(hostContainer), TYPE, GraphQLProvidersModuleDescriptor.class);
    }
}
