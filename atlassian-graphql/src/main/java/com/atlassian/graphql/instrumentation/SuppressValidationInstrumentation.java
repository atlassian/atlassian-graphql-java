package com.atlassian.graphql.instrumentation;

import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.SimpleInstrumentationContext;
import graphql.execution.instrumentation.parameters.InstrumentationValidationParameters;
import graphql.validation.ValidationError;
import graphql.validation.ValidationErrorType;

import java.util.List;
import java.util.Set;

/**
 * Provides instrumentation that suppresses specific query validation error types.
 */
public class SuppressValidationInstrumentation extends SimpleInstrumentation {
    private final Set<ValidationErrorType> suppressErrorTypes;

    public SuppressValidationInstrumentation(final Set<ValidationErrorType> suppressErrorTypes) {
        this.suppressErrorTypes = suppressErrorTypes;
    }

    @Override
    public InstrumentationContext<List<ValidationError>> beginValidation(
            final InstrumentationValidationParameters parameters) {

        return new SimpleInstrumentationContext<List<ValidationError>>() {
            @Override
            public void onCompleted(List<ValidationError> result, Throwable t) {
                result.removeIf(validationError -> suppressErrorTypes.contains(validationError.getValidationErrorType()));
            }
        };
    }
}
