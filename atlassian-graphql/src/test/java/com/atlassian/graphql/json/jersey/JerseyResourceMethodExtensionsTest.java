package com.atlassian.graphql.json.jersey;

import com.atlassian.graphql.GraphQLFetcherException;
import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.mock.jersey.Response;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class JerseyResourceMethodExtensionsTest {

    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(), EnumSet.noneOf(GraphQLJsonSerializerOptions.class));

    @SuppressWarnings("unchecked")
    @Test
    public void testJerseyResponseIsUnwrapped() throws Exception {
        final GraphQLTestTypeBuilderImpl typeBuilder =
                new GraphQLTestTypeBuilderImpl(new JerseyResourceMethodExtensions());
        final Map<String, List<String>> response =
                (Map<String, List<String>>)
                        serializer.serializeToObjectUsingGraphQL(typeBuilder, TestJerseyResource.class, new TestJerseyResource());

        List<String> list = response.get("value");
        assertEquals(2, list.size());
        assertEquals("item1", list.get(0));
        assertEquals("item2", list.get(1));
    }

    @Test
    public void testJerseyResponseErrorStatus() {
        final GraphQLTestTypeBuilderImpl typeBuilder =
                new GraphQLTestTypeBuilderImpl(new JerseyResourceMethodExtensions());
        try {
            serializer.serializeToObjectUsingGraphQL(typeBuilder, TestJerseyResourceWithError.class, new TestJerseyResourceWithError());
        } catch (Exception ex) {
            final GraphQLFetcherException restError = (GraphQLFetcherException) ex.getCause();
            assertEquals("Error code 403", restError.getMessage());
            assertEquals(403, ((GraphQLRestException) restError.getCause()).getStatusCode());
        }
    }

    private static class TestJerseyResource {
        @GraphQLName
        @com.atlassian.graphql.annotations.GraphQLType({List.class, String.class})
        public Response getValue() {
            return new Response() {
                @Override
                public Object getEntity() {
                    return Lists.newArrayList("item1", "item2");
                }
            };
        }
    }

    private static class TestJerseyResourceWithError {
        @GraphQLName
        @com.atlassian.graphql.annotations.GraphQLType({List.class, String.class})
        public Response getValue() {
            return new Response() {
                @Override
                public Object getEntity() {
                    return null;
                }

                @Override
                public int getStatus() {
                    return 403;
                }
            };
        }
    }
}