package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.expansions.GraphQLExpandable;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.types.RootGraphQLTypeBuilder;
import com.google.common.collect.Lists;
import graphql.language.Document;
import graphql.language.Field;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static graphql.schema.DataFetchingEnvironmentImpl.newDataFetchingEnvironment;
import static org.junit.Assert.assertEquals;

public class GraphQLExpansionEvaluatorTest {
    private final DataFetchingEnvironment env = newDataFetchingEnvironment().variables(Collections.emptyMap()).build();

    @Test
    public void testEvaluateExpansions() {
        GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "  content(offset: 0, first: 25) {",
                "    unrelatedThing {",
                "      unrelatedField",
                "    }",
                "    edges {",
                "      cursor",
                "      node {",
                "        nodeField",
                "        recursiveReference {",
                "          nodeField",
                "        }",
                "      }",
                "    }",
                "  }",
                "}"));
        final Field contentField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(contentField, "edges.node", env));
    }

    @Test
    public void testEvaluateExpansionsOnSingleObject() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(nodeField, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithOverride() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestExpandable.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "  field {",
                "   id",
                "   node {",
                "        nodeField",
                "        recursiveReference {",
                "          nodeField",
                "        }",
                "      }",
                "   }",
                "}"));
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("custom.expand.nodeField", "custom.expand.recursiveReference.nodeField"),
                evaluator.evaluateExpansions(nodeField, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithIgnore() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestIgnore.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "  field {",
                "   id",
                "   node {",
                "        nodeField",
                "        recursiveReference {",
                "          nodeField",
                "        }",
                "      }",
                "   }",
                "}"));
        final Field field = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(field, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithIgnoreAsMethod() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestIgnoreAsMethod.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "  field {",
                "   id",
                "   node {",
                "        nodeField",
                "        recursiveReference {",
                "          nodeField",
                "        }",
                "      }",
                "   }",
                "}"));
        final Field field = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(field, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithIncludeDirectiveVariableTrue() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @include(if: $isInclude) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        Map<String, Object> variablesMap = new HashMap();
        variablesMap.put("isInclude", Boolean.TRUE);
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(nodeField, "", newDataFetchingEnvironment().variables(variablesMap).build()));
    }

    @Test
    public void testEvaluateExpansionsWithIncludeDirectiveVariableFalse() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @include(if: $isInclude) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        Map<String, Object> variablesMap = new HashMap();
        variablesMap.put("isInclude", Boolean.FALSE);
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField"),
                evaluator.evaluateExpansions(nodeField, "", newDataFetchingEnvironment().variables(variablesMap).build()));
    }

    @Test
    public void testEvaluateExpansionsWithIncludeDirectiveFalse() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @include(if: false) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField"),
                evaluator.evaluateExpansions(nodeField, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithIncludeDirectiveTrue() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @include(if: true) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(nodeField, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithSkipDirectiveVariableFalse() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @skip(if: $isSkip) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        Map<String, Object> variablesMap = new HashMap();
        variablesMap.put("isSkip", Boolean.FALSE);
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(nodeField, "", newDataFetchingEnvironment().variables(variablesMap).build()));
    }

    @Test
    public void testEvaluateExpansionsWithSkipDirectiveVariableTrue() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @skip(if: $isSkip) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        Map<String, Object> variablesMap = new HashMap();
        variablesMap.put("isSkip", Boolean.TRUE);
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField"),
                evaluator.evaluateExpansions(nodeField, "", newDataFetchingEnvironment().variables(variablesMap).build()));
    }

    @Test
    public void testEvaluateExpansionsWithSkipDirectiveFalse() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @skip(if: false) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField", "recursiveReference.nodeField"),
                evaluator.evaluateExpansions(nodeField, "", env));
    }

    @Test
    public void testEvaluateExpansionsWithSkipDirectiveTrue() {
        final GraphQLExpansionEvaluator evaluator = buildEvaluator(TestSingleSource.class);
        final Document query = new graphql.parser.Parser().parseDocument(String.join("\n", "",
                "{",
                "      node {",
                "        nodeField",
                "        recursiveReference @skip(if: true) {",
                "          nodeField",
                "        }",
                "      }",
                "}"));
        final Field nodeField = (Field) ((graphql.language.Node) query.getChildren().get(0).getChildren().get(0)).getChildren().get(0);
        assertEquals(
                Lists.newArrayList("nodeField"),
                evaluator.evaluateExpansions(nodeField, "", env));
    }

    private GraphQLExpansionEvaluator buildEvaluator(Class type) {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLObjectType providerType = (GraphQLObjectType) typeBuilder.buildType(type, context);
        final GraphQLOutputType fieldType = providerType.getFieldDefinitions().get(0).getType();

        return new GraphQLExpansionEvaluator(
                fieldType,
                Collections.emptyMap(),
                context.getTypes());
    }

    public static class TestSource {
        @GraphQLName("content")
        private PageResponse content;
    }

    public static class PageResponse {
        @GraphQLName("unrelatedThing")
        private List<UnrelatedObject> unrelatedThing;

        @GraphQLName("edges")
        private List<Edge> edges;
    }

    public static class Edge {
        @GraphQLName
        private String cursor;

        @GraphQLName
        private Node node;
    }

    public static class Node {
        @GraphQLName("nodeField")
        @GraphQLExpandable
        private String nodeField;

        @GraphQLName("recursiveReference")
        @GraphQLExpandable
        private Node recursiveReference;
    }

    public static class UnrelatedObject {
        @GraphQLName("unrelatedField")
        @GraphQLExpandable
        private String unrelatedField;
    }

    public static class TestSingleSource {
        @GraphQLName("node")
        private Node node;
    }

    public static class TestExpandable {
        @GraphQLName("field")
        private ExpandableNode field;
    }

    public static class TestIgnore {
        @GraphQLName("field")
        private IgnoreNode field;
    }

    public static class TestIgnoreAsMethod {
        @GraphQLName("field")
        public IgnoreMethodNode getField() {
            return new IgnoreMethodNode();
        }
    }

    public static class ExpandableNode {
        @GraphQLName("id")
        private String id;

        @GraphQLName("node")
        @GraphQLExpandable("custom.expand")
        private Node node;
    }

    public static class IgnoreNode {
        @GraphQLName("id")
        private String id;

        @GraphQLName("node")
        @GraphQLExpandable(skip = true)
        private Node node;
    }

    public static class IgnoreMethodNode {
        @GraphQLName("id")
        public String getId() {
            return "blah";
        }

        @GraphQLName("node")
        @GraphQLExpandable(skip = true)
        public Node getNode() {
            return new Node();
        }
    }

}
