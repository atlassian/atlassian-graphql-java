package com.atlassian.graphql.types;

import com.atlassian.graphql.datafetcher.FieldDataFetcher;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.DataFetcherFactory;
import com.atlassian.graphql.utils.ObjectTypeBuilderHelper;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * A GraphQLObjectType builder for Map objects that have their field schema defined at runtime.
 */
public class DynamicTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;
    private final GraphQLExtensions extensions;

    public DynamicTypeBuilder(final GraphQLTypeBuilder typeBuilder, final GraphQLExtensions extensions) {
        requireNonNull(typeBuilder);
        this.typeBuilder = typeBuilder;
        this.extensions = extensions;
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return ObjectTypeBuilderHelper.buildDefaultTypeName(type, element, context, extensions);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return type instanceof DynamicType;
    }

    /**
     * Build a {@link GraphQLObjectType} object from a Map objects represented by {@link DynamicType}
     * that have their field schema defined at runtime.
     * @param typeName The name to give the type
     * @param type A {@link DynamicType} with its fields containing a Scalar type, {@link DynamicType} representing nested objects.
     *             NOTE: To represent a Non nullable field, wrap its original type with {@link NonNullWrapperType} type.
     * @param element The field, method or parameter
     * @param context Provides contextual information, including types built so far
     * @return The built {@link GraphQLType}
     */
    @Override
    public GraphQLType buildType(String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);

        // see if the type has already been built
        if (context.hasTypeOrIsBuilding(typeName)) {
            return new GraphQLTypeReference(typeName);
        }
        context.typeBuildStarted(typeName);

        final GraphQLObjectType.Builder builder =
                GraphQLObjectType.newObject()
                .name(typeName);

        // allow extensions to contribute new fields and type name
        final LinkedHashMap<String, GraphQLFieldDefinition> fields = new LinkedHashMap<>();
        if (extensions != null) {
            final List<GraphQLFieldDefinition> extensionFields = new ArrayList<>();
            extensions.contributeFields(typeName, type, extensionFields, context);
            for (final GraphQLFieldDefinition field : extensionFields) {
                fields.put(field.getName(), field);
            }
        }

        // build the primary fields
        buildPrimaryFields((DynamicType) type, context, fields);

        if (fields.isEmpty()) {
            return null;
        }
        for (final GraphQLFieldDefinition field : fields.values()) {
            builder.field(field);
        }

        // build the type
        final GraphQLObjectType graphqlType = builder.build();
        context.addType(graphqlType);
        return graphqlType;
    }

    private void buildPrimaryFields(
            final DynamicType dynamicType,
            final GraphQLTypeBuilderContext context,
            LinkedHashMap<String, GraphQLFieldDefinition> fields) {
        for (final Map.Entry<String, Type> field : dynamicType.getFieldTypes().entrySet()) {
            if (!fields.containsKey(field.getKey())) {
                final GraphQLFieldDefinition fieldDefinition = buildField(field.getKey(), field.getValue(), context);
                if (fieldDefinition != null) {
                    fields.put(fieldDefinition.getName(), fieldDefinition);
                }
            }
        }
    }

    private GraphQLFieldDefinition buildField(
            final String fieldName,
            final Type wrappedFieldType,
            final GraphQLTypeBuilderContext context) {

        boolean isNonNullableField = wrappedFieldType instanceof NonNullWrapperType;
        final Type unwrappedFieldType = isNonNullableField ? ((NonNullWrapperType) wrappedFieldType).getWrappedType() : wrappedFieldType;

        // build the field type
        context.enterField(fieldName, unwrappedFieldType);
        GraphQLOutputType graphqlFieldType = (GraphQLOutputType) typeBuilder.buildType(unwrappedFieldType, null, context);
        context.exitField();
        if (graphqlFieldType == null) {
            return null;
        }

        // build the field
        return GraphQLFieldDefinition.newFieldDefinition()
               .name(fieldName)
               .type(isNonNullableField ? GraphQLNonNull.nonNull(graphqlFieldType) : graphqlFieldType)
               .dataFetcher(createDataFetcher(fieldName, unwrappedFieldType, graphqlFieldType, context))
               .build();
    }

    private DataFetcher createDataFetcher(
            final String fieldName,
            final Type fieldType,
            final GraphQLOutputType graphqlFieldType,
            final GraphQLTypeBuilderContext context) {

        return DataFetcherFactory.createDataFetcherAndValueTransformer(
                () -> new FieldDataFetcher(fieldName, null),
                fieldName,
                fieldType,
                null,
                null,
                graphqlFieldType,
                typeBuilder,
                context,
                extensions);
    }
}
