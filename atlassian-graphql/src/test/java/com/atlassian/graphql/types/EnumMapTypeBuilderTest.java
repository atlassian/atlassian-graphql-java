package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLType;
import com.atlassian.graphql.json.types.JsonRootGraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.collect.ImmutableMap;
import graphql.execution.ExecutionContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingEnvironmentImpl;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;


public class EnumMapTypeBuilderTest {
    @SuppressWarnings("unchecked")
    @Test
    public void testBuildTypeAndFetchData() throws Exception {
        // build the type
        final JsonRootGraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(
                ObjectWithMapField.class,
                new GraphQLTypeBuilderContext());
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        final GraphQLFieldDefinition field = type.getFieldDefinition("map");

        // create the source
        final Map<KeyEnum, String> map = ImmutableMap.of(KeyEnum.KEY1, "value1", KeyEnum.KEY2, "value2");
        final ObjectWithMapField source = new ObjectWithMapField(map);

        // fetch the field value
        final DataFetchingEnvironment data =
                DataFetchingEnvironmentImpl.newDataFetchingEnvironment()
                .source(source)
                .fieldDefinition(field)
                .fieldType(field.getType())
                .context(mock(ExecutionContext.class))
                .build();
        final Map<String, String> response = (Map<String, String>) schema.getCodeRegistry().getDataFetcher(type, field).get(data);

        // assert
        assertEquals(2, response.size());
        assertEquals("value1", response.get("key1"));
        assertEquals("value2", response.get("KEY2"));
    }

    public static class ObjectWithMapField {
        @GraphQLName
        @GraphQLType(EnumMapTypeBuilder.class)
        private Map<KeyEnum, String> map;

        public ObjectWithMapField(final Map<KeyEnum, String> map) {
            this.map = map;
        }
    }

    public static enum KeyEnum {
        @GraphQLName("key1")
        KEY1,
        KEY2,
    }
}
