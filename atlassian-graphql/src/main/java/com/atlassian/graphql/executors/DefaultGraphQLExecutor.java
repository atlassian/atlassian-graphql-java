package com.atlassian.graphql.executors;

import com.atlassian.graphql.GraphQLExecutor;
import com.atlassian.graphql.rest.GraphQLRestRequest;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;

import java.io.IOException;

/**
 * A default implementation of {@link GraphQLExecutor} that delegates to {@link GraphQL}.
 */
public class DefaultGraphQLExecutor implements GraphQLExecutor {
    private final GraphQL graphql;

    public DefaultGraphQLExecutor(final GraphQL graphql) {
        this.graphql = graphql;
    }

    public ExecutionResult execute(final GraphQLRestRequest request, final Object context) throws IOException {
        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
                .query(request.getQuery())
                .operationName(request.getOperationName())
                .context(context)
                .variables(request.getVariables())
                .build();
        return graphql.execute(executionInput);
    }
}
