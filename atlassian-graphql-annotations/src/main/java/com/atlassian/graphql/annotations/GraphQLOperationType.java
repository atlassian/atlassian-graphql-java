package com.atlassian.graphql.annotations;

public enum GraphQLOperationType {
    QUERY,
    MUTATION
}
