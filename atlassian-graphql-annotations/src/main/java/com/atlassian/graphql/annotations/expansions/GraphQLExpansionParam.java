package com.atlassian.graphql.annotations.expansions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Binds a comma-separated list of expansion fields to a REST API provider method ('expand') parameter.
 * The expansion properties are reflected from the type of the object returned from the provider method.
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphQLExpansionParam {
}
