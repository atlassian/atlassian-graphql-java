package com.atlassian.graphql.provider;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.annotations.GraphQLMappedBatchLoader;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.annotations.GraphQLBatchLoader;
import com.atlassian.graphql.spi.GraphQLExtensions;
import graphql.ExecutionResult;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.dataloader.DataLoaderDispatcherInstrumentation;
import graphql.execution.instrumentation.parameters.InstrumentationExecutionParameters;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.dataloader.MappedBatchLoader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import static com.atlassian.graphql.utils.AnnotationsHelper.getAnnotation;
import static com.atlassian.graphql.utils.AnnotationsHelper.hasAnnotation;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;


/**
 * Adds DataLoader support by scanning providers for methods marked with {@link GraphQLBatchLoader}
 */
public class DataLoaderGraphQLExtensions implements GraphQLExtensions {

    private final List<Consumer<DataLoaderRegistry>> registrations;

    public DataLoaderGraphQLExtensions(List<GraphQLExtensions> extensions, List<GraphQLProviders> providerContainers) {
        List<Object> providers = concat(
                providerContainers
                        .stream()
                        .flatMap(p -> p.getProviders().stream()),
                extensions.stream())
                .collect(toList());

        this.registrations = loadRegistrationsFromProviders(providers);
    }

    List<Consumer<DataLoaderRegistry>> loadRegistrationsFromProviders(List<Object> providers) {
        List<Consumer<DataLoaderRegistry>> registrations = new ArrayList<>();
        Set<String> registeredMethodIds = new HashSet<>();
        for (Object provider : providers) {
            for (Method method : Arrays.stream(provider.getClass().getMethods())
                    .filter(m -> hasAnnotation(m, GraphQLBatchLoader.class) || hasAnnotation(m, GraphQLMappedBatchLoader.class))
                    .collect(toList())) {
                String id = methodId(method);
                if (registeredMethodIds.contains(id)) {
                    throw new IllegalArgumentException("Duplicate BatchLoader or MappedBatchLoader id '" + id + "' detected");
                } else {
                    registeredMethodIds.add(id);
                }

                //noinspection unchecked
                registrations.add(registry -> {
                    DataLoader dataLoader = hasAnnotation(method, GraphQLBatchLoader.class)
                            ? DataLoader.newDataLoader(newBatchLoader(provider, method))
                            : DataLoader.newMappedDataLoader(newMappedBatchLoader(provider, method));

                    registry.register(id, dataLoader);
                });
            }
        }
        return unmodifiableList(registrations);
    }

    @Override
    public Instrumentation getInstrumentation() {
        if (!registrations.isEmpty()) {
            return new ChainedInstrumentation(asList(
                    new SimpleInstrumentation() {
                        @Override
                        public InstrumentationContext<ExecutionResult> beginExecution(InstrumentationExecutionParameters parameters) {
                            return super.beginExecution(parameters);
                        }
                    },
                    new DataLoaderDispatcherInstrumentation()
            ));
        } else {
            return SimpleInstrumentation.INSTANCE;
        }
    }

    private static BatchLoader newBatchLoader(Object provider, Method method) {
        try {
            return (BatchLoader) method.invoke(provider);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static MappedBatchLoader newMappedBatchLoader(Object provider, Method method) {
        try {
            return (MappedBatchLoader) method.invoke(provider);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static String methodId(Method m) {
        String value;
        if (hasAnnotation(m, GraphQLBatchLoader.class)) {
            value = getAnnotation(m, GraphQLBatchLoader.class).value();
        } else {
            value = getAnnotation(m, GraphQLMappedBatchLoader.class).value();
        }

        return value.isEmpty() ? m.getName() : value;
    }

    public void populateDataLoaderRegistry(DataLoaderRegistry registry, Object context) {
        if (context instanceof GraphQLContext) {
            if (!registrations.isEmpty()) {
                registrations.forEach(reg -> reg.accept(registry));
                ((GraphQLContext) context).injectParameterValue(DataLoaderRegistry.class, registry);
                ((GraphQLContext) context).setRegistry(registry);
            }
        } else {
            throw new IllegalArgumentException("Cannot use a different context class than "
                    + GraphQLContext.class.getName() + " if BatchLoaders are present in Provider classes.");
        }
    }
}
