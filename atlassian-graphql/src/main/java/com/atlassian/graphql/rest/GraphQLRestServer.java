package com.atlassian.graphql.rest;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.provider.DataLoaderGraphQLExtensions;
import com.atlassian.graphql.provider.RootProviderGraphQLTypeBuilder;
import com.atlassian.graphql.provider.internal.extensions.GraphQLExtensionsFactory;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.types.RootGraphQLTypeBuilder;
import com.atlassian.graphql.utils.AsyncExecutionStrategyWithExecutionListenerSupport;
import com.atlassian.graphql.utils.AsyncSerialExecutionStrategyWithExecutionListenerSupport;
import com.atlassian.graphql.utils.GraphQLUtils;
import com.google.common.collect.Lists;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.execution.ExecutionStrategy;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.schema.GraphQLSchema;
import org.dataloader.DataLoaderRegistry;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.atlassian.graphql.spi.CombinedGraphQLExtensions.combine;
import static java.util.Objects.requireNonNull;

/**
 * Provides a REST API endpoint for GraphQL for a set of annotated provider classes and extensions.
 */
public class GraphQLRestServer {
    private final GraphQLSchema schema;
    private final GraphQLRestEndpoint executor;

    private GraphQLRestServer(
            final String queryTypeName,
            final String mutationTypeName,
            final Function<GraphQLExtensions, GraphQLTypeBuilder> typeBuilderSupplier,
            final List<GraphQLProviders> providers,
            final GraphQLExtensions extensions,
            final ExecutionStrategy queryExecutionStrategy,
            final ExecutionStrategy mutationExecutionStrategy,
            final Consumer<List<GraphQLRestRequest>> beforeRequest,
            final BiConsumer<Object, Exception> schemaBuildErrorHandler,
            final BiFunction<GraphQLRestRequest, Exception, Object> errorConverter,
            final BiConsumer<GraphQLRestRequest, Exception> queryExceptionHandler) {

        requireNonNull(typeBuilderSupplier);
        requireNonNull(providers);

        final RootProviderGraphQLTypeBuilder typeBuilder = new RootProviderGraphQLTypeBuilder(typeBuilderSupplier, extensions);

        final List<GraphQLExtensions> providerExtensions = GraphQLExtensionsFactory.getProviderGraphQLExtensions(providers);
        final DataLoaderGraphQLExtensions dataLoaderExtensions = new DataLoaderGraphQLExtensions(providerExtensions, providers);
        final GraphQLExtensions allExtensions = combine(extensions, dataLoaderExtensions, combine(providerExtensions));

        this.schema = typeBuilder.buildSchema(queryTypeName, mutationTypeName, providers, schemaBuildErrorHandler);
        this.executor = new GraphQLRestEndpoint(
                (request, context) -> {
                    DataLoaderRegistry dataLoaderRegistry = new DataLoaderRegistry();
                    dataLoaderExtensions.populateDataLoaderRegistry(dataLoaderRegistry, context);
                    ExecutionInput executionInput = ExecutionInput.newExecutionInput(request.getQuery())
                            .operationName(request.getOperationName())
                            .context(context)
                            .variables(request.getVariables())
                            .dataLoaderRegistry(dataLoaderRegistry)
                            .build();

                    return join(GraphQLUtils.executeDocument(
                            schema,
                            request.getQueryDocument(),
                            executionInput,
                            queryExecutionStrategy != null ? queryExecutionStrategy : new AsyncExecutionStrategyWithExecutionListenerSupport(),
                            mutationExecutionStrategy != null ? mutationExecutionStrategy : new AsyncSerialExecutionStrategyWithExecutionListenerSupport(),
                            allExtensions != null ? allExtensions.getInstrumentation() : SimpleInstrumentation.INSTANCE));
                },
                errorConverter) {

            @Override
            public List<Map<String, Object>> executeList(final List<GraphQLRestRequest> requests, final GraphQLContext context)
                    throws IOException {

                beforeRequest.accept(requests);
                return super.executeList(requests, context);
            }

            @Override
            public Map<String, Object> execute(final GraphQLRestRequest request, final GraphQLContext context)
                    throws IOException {

                beforeRequest.accept(Collections.singletonList(request));
                if (queryExceptionHandler != null) {
                    try {
                        return super.execute(request, context);
                    } catch (Exception ex) {
                        queryExceptionHandler.accept(request, ex);
                        throw ex;
                    }
                } else {
                    return super.execute(request, context);
                }
            }
        };
    }

    /**
     * Get a builder for a {@link GraphQLRestServer}.
     */
    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String queryTypeName = "Query";
        private String mutationTypeName = "Mutation";
        private Function<GraphQLExtensions, GraphQLTypeBuilder> typeBuilderSupplier = RootGraphQLTypeBuilder::new;
        private List<GraphQLProviders> providers = Lists.newArrayList();
        private GraphQLExtensions extensions;
        private ExecutionStrategy queryExecutionStrategy;
        private ExecutionStrategy mutationExecutionStrategy;
        private Consumer<List<GraphQLRestRequest>> beforeRequest = requests -> {};
        private BiConsumer<Object, Exception> schemaBuildErrorHandler;
        private BiFunction<GraphQLRestRequest, Exception, Object> errorConverter;
        private BiConsumer<GraphQLRestRequest, Exception> queryExceptionHandler;

        /**
         * The root query type name of the graphql schema.
         */
        public Builder queryTypeName(final String queryTypeName) {
            this.queryTypeName = queryTypeName;
            return this;
        }

        /**
         * The root mutation type name of the graphql schema.
         */
        public Builder mutationTypeName(final String mutationTypeName) {
            this.queryTypeName = mutationTypeName;
            return this;
        }

        /**
         * Provides the {@link GraphQLTypeBuilder} that's used to build GraphQL types.
         */
        public Builder typeBuilderSupplier(final Function<GraphQLExtensions, GraphQLTypeBuilder> typeBuilderSupplier) {
            this.typeBuilderSupplier = typeBuilderSupplier;
            return this;
        }

        /**
         * Add a graphql annotated provider object.
         */
        public Builder provider(final GraphQLProviders provider) {
            this.providers.add(provider);
            return this;
        }

        /**
         * The graphql annotated provider objects.
         */
        public Builder providers(final List<GraphQLProviders> providers) {
            this.providers.addAll(providers);
            return this;
        }

        /**
         * Provides extensions to the graph-ql type system.
         */
        public Builder extensions(final GraphQLExtensions extensions) {
            this.extensions = extensions;
            return this;
        }

        /**
         * Provides extensions to the graph-ql type system.
         */
        public Builder extensions(final List<GraphQLExtensions> extensions) {
            this.extensions = combine(extensions);
            return this;
        }

        /**
         * Provides the query {@link ExecutionStrategy}.
         */
        public Builder queryExecutionStrategy(final ExecutionStrategy queryExecutionStrategy) {
            this.queryExecutionStrategy = queryExecutionStrategy;
            return this;
        }

        /**
         * Provides the mutation {@link ExecutionStrategy}.
         */
        public Builder mutationExecutionStrategy(final ExecutionStrategy mutationExecutionStrategy) {
            this.mutationExecutionStrategy = mutationExecutionStrategy;
            return this;
        }

        /**
         * Invoked before a request has started. May be used to provide query throttling.
         */
        public Builder beforeRequest(final Consumer<List<GraphQLRestRequest>> beforeRequest) {
            this.beforeRequest = beforeRequest;
            return this;
        }

        /**
         * Invoked when an error happens while building the schema for a provider.
         */
        public Builder schemaBuildErrorHandler(final BiConsumer<Object, Exception> schemaBuildErrorHandler) {
            this.schemaBuildErrorHandler = schemaBuildErrorHandler;
            return this;
        }

        /**
         * Converts an error before applying it to the response.
         */
        public Builder errorConverter(final BiFunction<GraphQLRestRequest, Exception, Object> errorConverter) {
            this.errorConverter = errorConverter;
            return this;
        }

        /**
         * Called when executing an individual GraphQL query results in an exception.
         */
        public Builder queryExceptionHandler(final BiConsumer<GraphQLRestRequest, Exception> queryExceptionHandler) {
            this.queryExceptionHandler = queryExceptionHandler;
            return this;
        }

        public GraphQLRestServer build() {
            return new GraphQLRestServer(
                    queryTypeName,
                    mutationTypeName,
                    typeBuilderSupplier,
                    providers,
                    extensions,
                    queryExecutionStrategy,
                    mutationExecutionStrategy,
                    beforeRequest,
                    schemaBuildErrorHandler,
                    errorConverter,
                    queryExceptionHandler);
        }
    }

    private static ExecutionResult join(final CompletableFuture<ExecutionResult> executionResult) {
        try {
            return executionResult.join();
        } catch (CompletionException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            } else {
                throw e;
            }
        }
    }

    /**
     * Get the {@link GraphQLSchema} that describes the endpoint.
     */
    public GraphQLSchema getSchema() {
        return schema;
    }

    /**
     * Execute a graph-ql REST request.
     * @param requestString The graph-ql request in mime application/graphql format
     * @param context The context variables to pass to provider methods
     * @return The REST response, usually a map that may be serialized using jackson
     * @throws IOException If there was an error executing the request
     */
    public Object execute(final String requestString, final GraphQLContext context) throws IOException {
        return executor.execute(requestString, context);
    }

    /**
     * Execute a graph-ql REST request as an array of requests.
     * @param requests The graph-ql requests
     * @param context The context variables to pass to provider methods
     * @return The REST response, usually a map that may be serialized using jackson
     * @throws IOException If there was an error executing the request
     */
    public List<Map<String, Object>> executeList(final List<GraphQLRestRequest> requests, final GraphQLContext context) throws IOException {
        return executor.executeList(requests, context);
    }

    /**
     * Execute a graph-ql REST request.
     * @param request The graph-ql request
     * @param context The context variables to pass to provider methods
     * @return The REST response
     * @throws IOException If there was an error executing the request
     */
    public Map<String, Object> execute(final GraphQLRestRequest request, final GraphQLContext context) throws IOException {
        return executor.execute(request, context);
    }
}
