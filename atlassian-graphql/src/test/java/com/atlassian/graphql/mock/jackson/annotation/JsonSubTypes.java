package com.atlassian.graphql.mock.jackson.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonSubTypes {
    JsonSubTypes.Type[] value();

    @interface Type {
        Class<?> value();

        String name() default "";
    }
}
