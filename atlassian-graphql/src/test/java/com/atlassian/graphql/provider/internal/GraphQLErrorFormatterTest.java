package com.atlassian.graphql.provider.internal;

import com.atlassian.graphql.GraphQLFetcherException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.InvalidSyntaxError;
import graphql.execution.ResultPath;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;

public class GraphQLErrorFormatterTest {
    @Test
    public void testFormatErrorsMap_ProviderException() {
        final Exception error1 = new GraphQLFetcherException(ResultPath.rootPath().segment("providerName1"), new RuntimeException("badness"));
        final Exception error2 = new GraphQLFetcherException(ResultPath.rootPath().segment("providerName2"), new IllegalArgumentException("badness"));

        final List<Object> expected = Lists.newArrayList(
                ImmutableMap.of("path", Lists.newArrayList("providerName1"), "message", "java.lang.RuntimeException; badness"),
                ImmutableMap.of("path", Lists.newArrayList("providerName2"), "message", "java.lang.IllegalArgumentException; badness"));
        final List<Object> actual = GraphQLErrorFormatter.formatErrorsMap(
                Lists.newArrayList(new ExceptionWhileDataFetching(ResultPath.rootPath(), error1, null), new ExceptionWhileDataFetching(ResultPath.rootPath(), error2, null)),
                null);
        assertEquals(expected, actual);
    }

    @Test
    public void testFormatErrorsMap_ArbitraryErrors() {
        final GraphQLError error1 = new ExceptionWhileDataFetching(ResultPath.rootPath(), new IllegalArgumentException("badness"), null);
        final GraphQLError error2 = new InvalidSyntaxError(emptyList(), null) {
            @Override
            public String getMessage() {
                return "Error message";
            }
        };
        final List<Object> errors = GraphQLErrorFormatter.formatErrorsMap(
                Lists.newArrayList(error1, error2),
                null);

        assertEquals(2, errors.size());
        assertEquals(
                "Exception name returned for arbitrary exception errors",
                true, ((String) ((Map) errors.get(0)).get("message")).contains("IllegalArgumentException; badness"));
        assertEquals("Message returned for other graphql errors", "Error message", ((Map) errors.get(1)).get("message"));
    }

    @Test
    public void testFormatErrorsMap_CustomExceptions() {

        final GraphQLError error = new ExceptionWhileDataFetching(ResultPath.rootPath(), new CustomException("random message", "other message"), null);

        final List<Object> errors = GraphQLErrorFormatter.formatErrorsMap(
                Lists.newArrayList(error),
                e -> {
                    final ObjectMapper mapper = new ObjectMapper();
                    return mapper.convertValue(e, Map.class);
                });

        assertEquals(
                "Message returned contains custom attributes",
                true, ((Map) errors.get(0)).get("otherMessage").equals("other message"));
    }

    private static class CustomException extends RuntimeException {

        public final String otherMessage;

        public CustomException(String message, String otherMessage) {
            super(message);
            this.otherMessage = otherMessage;
        }
    }
}
